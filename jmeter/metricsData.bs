import es.kibu.geoapis.HistoryDataProducer;
import es.kibu.geoapis.Location;
import es.kibu.geoapis.LocationHistory;
import es.kibu.geoapis.data.DataCreator;
import es.kibu.geoapis.data.DataUtils;
import es.kibu.geoapis.data.providers.components.DaggerRandomGeneratorMaker;
import es.kibu.geoapis.data.providers.components.GeneratorsMaker;
import es.kibu.geoapis.data.providers.modules.RandomProvidersModule;
import es.kibu.geoapis.objectmodel.MetricsDefinition;
import es.kibu.geoapis.metrics.MetricsCoreUtils;
import es.kibu.geoapis.objectmodel.dimensions.Constants;


log.info("BEGIN invoking beanshell");

String metricDefinitionFile = "sample-metrics/sample-metric-real-filters-scopes.json";

log.info(MetricsCoreUtils.class.getSimpleName());

try {
	MetricsDefinition metricsDefinition = MetricsCoreUtils.withMetricDefinition(metricDefinitionFile);
}
catch(Exception e) {
   log.info(e.getMessage());	
}

Map<String, List<JsonElement>> variablesAndData = DataUtils.generateListOfElements(metricsDefinition, 200, dataGenerator);

vars.put("data", "hello");

log.info("END invoking beanshell");
