import es.kibu.geoapis.HistoryDataProducer;
import es.kibu.geoapis.Location;
import es.kibu.geoapis.LocationHistory;
import es.kibu.geoapis.data.DataCreator;
import es.kibu.geoapis.data.DataUtils;
import es.kibu.geoapis.data.providers.components.DaggerRandomGeneratorMaker;
import es.kibu.geoapis.data.providers.components.GeneratorsMaker;
import es.kibu.geoapis.data.providers.modules.RandomProvidersModule;
import es.kibu.geoapis.objectmodel.MetricsDefinition;
import es.kibu.geoapis.metrics.MetricsCoreUtils;
import es.kibu.geoapis.objectmodel.dimensions.Constants;
import es.kibu.geoapis.data.Utils;

log.info("BEGIN invoking beanshell");

log.info("setting previd..");
var previd = vars.get("timeid");
log.info("creating the timeid..");
var timeid = Utils.newTimeUUID().toString();

log.info("adding to vars ..");
vars.put("timeid", timeid);
vars.put("previd", previd);

log.info("END invoking beanshell");
