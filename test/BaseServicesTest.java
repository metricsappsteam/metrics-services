import com.fasterxml.jackson.databind.JsonNode;
import play.mvc.Http;
import play.mvc.Result;
import play.test.Helpers;

import static org.fest.assertions.Assertions.assertThat;
import static play.test.Helpers.contentAsString;

/**
 * Created by lrodr_000 on 16/01/2017.
 */
public abstract class BaseServicesTest {

    protected abstract String getBaseCreate();
    protected abstract String getBaseUpdate();
    protected abstract  String getBaseExist();
    protected abstract String getBaseSelect();
    protected abstract Object getBaseDelete();


    protected  String getBaseList() {
        String baseSelect = getBaseSelect();
        if (baseSelect.endsWith("/")) {
            baseSelect = baseSelect.substring(0, baseSelect.length()-1);
        }
        return baseSelect;
    }

}
