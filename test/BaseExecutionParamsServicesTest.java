import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import es.kibu.geoapis.services.objectmodel.results.*;
import org.joda.time.DateTime;
import org.junit.Test;
import play.libs.Json;

import java.io.IOException;

import static org.junit.Assert.assertTrue;

/**
 * Created by lrodriguyez2002cu on 04/01/2017.
 */
public abstract class BaseExecutionParamsServicesTest extends BaseServicesTest {
    public static final String BASE_CREATE = "/api/v1/create-exec-params/";
    public static final String BASE_UPDATE = "/api/v1/update-exec-params/";
    public static final String BASE_DELETE = "/api/v1/delete-exec-params/";
    public static final String BASE_SELECT = "/api/v1/exec-params/";
    public static final String BASE_EXIST = "/api/v1/exec-params-exist/";


    @Test
    public void testCreateMetric() throws IOException, ProcessingException {
        String applicationId = getCreatedApp();
        checkCreateExecutionParams(applicationId, 200);
    }


    private String getCreatedApp() throws ProcessingException, IOException {
        return TestUtils.withCreatedApp();
    }

    @Test
    public void creatingAMetricForTheSameAppTwiceShouldFail() throws IOException, ProcessingException {
        String applicationId = getCreatedApp();
        checkCreateExecutionParams(applicationId, 200);
        checkCreateExecutionParams(applicationId, 400);
    }

    private String getRandomAppId() {
        return String.format("sample-appid%s", Long.toString(DateTime.now().getMillis()));
    }

    @Test
    public void gettingExecutionParamsForANonExistingAppShouldFail() throws IOException, ProcessingException {

        String applicationId = getRandomAppId(); //it should be made up, thats why the random
        boolean isEmpty = checkGetExecutionParams(applicationId, 400);
        assertTrue(isEmpty);
    }

    @Test
    public void updatingAMetricForNonExistignAppShouldFail() throws IOException, ProcessingException {
        String applicationId = getRandomAppId(); //it should be made up, thats why the random
        checkUpdateForApp(applicationId, 400);
    }

    @Test
    public void testDelete() throws IOException, ProcessingException {
        String applicationId = getRandomAppId();
        checkDeleteExecutionParams(applicationId, 200, "delete_failed");
    }

    @Test
    public void testDeleteComplex() throws IOException, ProcessingException {
        String applicationId = getCreatedApp();
        checkCreateExecutionParams(applicationId, 200);
        checkDeleteExecutionParams(applicationId, 200, "deleted");
    }

   /* @Test
    public void testUpdateExecutionParams() throws IOException, ProcessingException {

        String applicationId = checkCreateApplication(200);
        checkUpdateForApp(applicationId, 200);

        String applicationId = checkCreateExecutionParams(200);
        checkUpdateForApp(applicationId, 200);

    }*/

    @Test
    public void testUpdateExecutionParamsAndDelete() throws IOException, Exception {

        String applicationId = getCreatedApp();
        try {
            //Log.d("CREATING_METRIC", "Creating metric for app: " + applicationId);
            checkCreateExecutionParams(applicationId, 200);

            checkUpdateForApp(applicationId, 200);

        } finally {
            checkDeleteExecutionParams(applicationId, 200, "deleted");
        }

    }

    @Test
    public void testExecutionParamsExists() throws IOException, ProcessingException {

        String applicationId = TestUtils.withCreatedApp();
        checkCreateExecutionParams(applicationId, 200);
        checkExecutionParamsExist(applicationId);
    }

    @Test
    public void testGetExecutionParams() throws IOException, ProcessingException {
        //String applicationId = "sample-appid";

        String applicationId = getCreatedApp();
        try {
            //Log.d("CREATING_METRIC", "Creating metric for app: " + applicationId);
            checkCreateExecutionParams(applicationId, 200);
            checkGetExecutionParams(applicationId, 200);

        } finally {
            checkDeleteExecutionParams(applicationId, 200, "deleted");
        }
    }


    private void checkExecutionParamsExist(String applicationId) throws ProcessingException, IOException {

        String content = getExecutionParamsExistResult(applicationId, 200);
        ResultValue<ExistResult> existResult = ResultValueReader.asExistResult(content);
        assertTrue(existResult.getResult().isResult());
        assertTrue(existResult.getResult().getId()!= null);
    }

    private boolean checkGetExecutionParams(String applicationId, int expectedCode) {

        String content = getGetMetricResult(applicationId, expectedCode);
        JsonNode parse = Json.parse(content);

        if (expectedCode == 200) {
            assertTrue(parse.has("result"));
            if (!parse.get("result").isTextual() && parse.get("result").has("content")) {
                assertTrue(parse.get("result").has("application"));
                assertTrue(parse.get("result").has("id"));
                assertTrue(parse.get("result").has("push_url"));
                assertTrue(parse.get("result").has("fcm_server_key"));
            }
        } else {
            return parse.get("errorMessage").isTextual() && parse.get("errorMessage").asText().contains("No execution params for app") /*parse.get("result").isTextual() && parse.get("result").asText().equalsIgnoreCase("empty")*/;
        }

        return false;
    }

    private void checkDeleteExecutionParams(String applicationId, int expectedCode, String resultDeleted) throws ProcessingException, IOException {

        String content = getDeleteExecutionParamsResult(applicationId, expectedCode);

        JsonNode parse = Json.parse(content);

        if (expectedCode == 200) {
            ResultValue<DeleteResult> deleteResultResultValue = ResultValueReader.asDeleteResult(content);
            assertTrue(deleteResultResultValue.getResult().getResult().equalsIgnoreCase(resultDeleted));

        } else {
            assertTrue(parse.has("error") && parse.get("error").asBoolean());
        }
    }

    private void checkUpdateForApp(String applicationId, int expectedCode) throws ProcessingException, IOException {

        ExecutionParams sampleExecutionParams = getSampleModifiedExecutionParams();
        JsonNode jsonNode = Json.toJson(sampleExecutionParams);
        String content = getUpdateResult(applicationId, jsonNode, expectedCode);

        JsonNode parse = Json.parse(content);

        if (expectedCode == 200) {
            ResultValue<UpdateResult> deleteResultResultValue = ResultValueReader.asUpdateResult(content);
            assertTrue(deleteResultResultValue.getResult().getResult().equalsIgnoreCase("updated"));
        } else {
            assertTrue(parse.get("error").asBoolean());
        }
    }

    private ExecutionParams getSampleExecutionParams() {
        return new ExecutionParams("application", "fcm_server_key", "push_url");
    }


    private ExecutionParams getSampleModifiedExecutionParams() {
        return new ExecutionParams("applicationModified", "fcm_server_keyModified", "push_urlModified");
    }


    private void checkCreateExecutionParams(String applicationId, int expectedCode) throws ProcessingException, IOException {


        ExecutionParams sampleExecutionParams = getSampleExecutionParams();
        JsonNode jsonNode = Json.toJson(sampleExecutionParams);

        String content = getCreateResult(applicationId, jsonNode, expectedCode);

        JsonNode parse = Json.parse(content);
        if (expectedCode == 200) {
            ResultValue<CreateResult> resultValue = ResultValueReader.asCreateResult(content);
            assertTrue(resultValue.getResult().getResult().equalsIgnoreCase("created"));
            assertTrue(resultValue.getResult().getId() != null);
            assertTrue(!resultValue.getResult().getId().isEmpty());

        } else {
            assertTrue(parse.has("error") && parse.get("error").asBoolean());
        }

    }

    protected abstract String getCreateResult(String applicationId, JsonNode jsonNode, int expectedCode);

    protected abstract String getUpdateResult(String applicationId, JsonNode jsonNode, int expectedCode);

    protected abstract String getExecutionParamsExistResult(String applicationId, int expectedCode);

    protected abstract String getGetMetricResult(String applicationId, int expectedCode);

    protected abstract String getDeleteExecutionParamsResult(String applicationId, int expectedCode);


    protected String getBaseExist() {
        return BASE_EXIST;
    }

    protected String getBaseSelect() {
        return BASE_SELECT;
    }

    @Override
    protected String getBaseList() {
        return BASE_SELECT;
    }

    protected String getBaseDelete() {
        return BASE_DELETE;
    }

    @Override
    protected String getBaseCreate() {
        return BASE_CREATE;
    }

    @Override
    protected String getBaseUpdate() {
        return BASE_UPDATE;
    }
}
