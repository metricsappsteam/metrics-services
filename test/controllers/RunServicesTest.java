package controllers;

import akka.stream.Materializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.kibu.geoapis.metrics.actors.messages.MetricsMessage;
import es.kibu.geoapis.metrics.engine.core.BasicMetricsContext;
import es.kibu.geoapis.metrics.engine.core.Constants;
import models.Utils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import play.Application;
import play.test.Helpers;

import static controllers.RunServices.getRemoteActorAddress;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by lrodriguez2002cu on 13/03/2017.
 */
public class RunServicesTest {

    @Before
    public void startPlay() {
        app = provideApplication();
        Helpers.start(app);
        mat = app.getWrappedApplication().materializer();
    }

    @After
    public void stopPlay() {
        if (app != null) {
            Helpers.stop(app);
            app = null;
        }
    }

    protected Application app;

    /**
     * The application's Akka streams Materializer.
     */
    protected Materializer mat;


    /**
     * Override this method to setup the application to use.
     * <p>
     * By default this will call the old {@link #provideFakeApplication() provideFakeApplication} method.
     *
     * @return The application to use
     */
    protected Application provideApplication() {
        return provideFakeApplication();
    }

    /**
     * Override this method to setup the fake application to use.
     *
     * @return The fake application to use
     * @deprecated use the new {@link #provideApplication() provideApplication} method instead.
     */
    @Deprecated
    protected Application provideFakeApplication() {
        return Helpers.fakeApplication();
    }


    @Test
    public void testRunParamsEqualsVariableMode() {

        String applicationId = "applicationId";
        String userId = "userId";
        String sessionId = "sessionId";
        String variable = "variable1";

        RunServices.RunParams runParams = new RunServices.RunParams();
        runParams.context = new BasicMetricsContext(applicationId, userId, sessionId);
        runParams.parameters = new RunServices.Parameters(variable, MetricsMessage.EvaluationMode.evImmediateOnly);


        RunServices.RunParams runParams1 = new RunServices.RunParams();
        runParams1.context = new BasicMetricsContext(applicationId, userId, sessionId);
        runParams1.parameters = new RunServices.Parameters(variable, MetricsMessage.EvaluationMode.evImmediateOnly);

        assertEquals(runParams, runParams1);

    }

    @Test
    public void testRemoteActorRef() {
        //not very useful test though
        String remoteActorAddress = getRemoteActorAddress(app.configuration());
        assertTrue(remoteActorAddress.endsWith(Constants.SCRIPT_ENGINE_FRONTEND_ACTOR_NAME));
    }

    @Test
    public void testRunParamsEqualsMetricsMode() {

        String applicationId = "applicationId";
        String userId = "userId";
        String sessionId = "sessionId";

        RunServices.RunParams runParams = new RunServices.RunParams();
        runParams.context = new BasicMetricsContext(applicationId, userId, sessionId);
        String metric = "metric1";
        runParams.parameters = new RunServices.Parameters(metric);


        RunServices.RunParams runParams1 = new RunServices.RunParams();
        runParams1.context = new BasicMetricsContext(applicationId, userId, sessionId);
        runParams1.parameters = new RunServices.Parameters(metric);

        assertEquals(runParams, runParams1);

    }

    @Test
    public void testRunParamsSerialization() throws Exception {

        ObjectMapper mapper = Utils.getMapper();

        String applicationId = "applicationId";
        String userId = "userId";
        String sessionId = "sessionId";
        String variable1 = "variable1";

        RunServices.RunParams runParams = new RunServices.RunParams();
        runParams.context = new BasicMetricsContext(applicationId, userId, sessionId);
        runParams.parameters = new RunServices.Parameters(variable1, MetricsMessage.EvaluationMode.evImmediateOnly);


        String json = mapper.writeValueAsString(runParams);

        RunServices.RunParams runParams1 = mapper.readValue(json, RunServices.RunParams.class);

        assertEquals(runParams, runParams1);

    }

    String sampleJson = "{\n" +
            "  \"context\": {\n" +
            "    \"applicationId\": \"app1\",\n" +
            "    \"sessionId\": \"session1\",\n" +
            "    \"userId\": \"user1\"\n" +
            "  },\n" +
            "  \"parameters\": {\n" +
            "    \"evaluationMode\": \"evInmediateOnly\",\n" +
            "    \"variable\": \"variable1\",\n" +
            "    \"mode\": \"variable_based\"\n" +
            "  }\n" +
            "}";


    String sampleJson1 = "{\n" +
            "  \"context\": {\n" +
            "    \"applicationId\": \"app1\",\n" +
            "    \"sessionId\": \"session1\",\n" +
            "    \"userId\": \"user1\"\n" +
            "  },\n" +
            "  \"parameters\": {\n" +
            "    \"metric\": \"metric1\",\n" +
            "    \"mode\": \"metrics_based\"\n" +
            "  }\n" +
            "}";

    @Test
    public void testRunParamsSerializationWorksWithSampleJson() throws Exception {
        ObjectMapper mapper = Utils.getMapper();
        RunServices.RunParams runParams = mapper.readValue(sampleJson, RunServices.RunParams.class);

        assertEquals(runParams.context.getApplicationId(), "app1");
        assertEquals(runParams.context.getSessionId(), "session1");
        assertEquals(runParams.context.getUserId(), "user1");

        assertEquals(MetricsMessage.EvaluationMode.evImmediateOnly, runParams.parameters.evaluationMode);
        assertEquals("variable1", runParams.parameters.variable);
        assertEquals(RunServices.Mode.variable_based, runParams.parameters.getMode());
        assertEquals(MetricsMessage.EvaluationMode.evImmediateOnly, runParams.parameters.evaluationMode);
    }

    @Test
    public void testRunParamsSerializationWorksWithSampleJson1() throws Exception {

        ObjectMapper mapper = Utils.getMapper();

        RunServices.RunParams runParams = mapper.readValue(sampleJson1, RunServices.RunParams.class);

        assertEquals(runParams.context.getApplicationId(), "app1");
        assertEquals(runParams.context.getSessionId(), "session1");
        assertEquals(runParams.context.getUserId(), "user1");

        assertEquals("metric1", runParams.parameters.metric);
        assertEquals(RunServices.Mode.metrics_based, runParams.parameters.getMode());
        assertEquals(null, runParams.parameters.evaluationMode);

    }

}