import akka.stream.Materializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import play.Application;
import play.mvc.Http;
import play.mvc.Result;
import play.test.Helpers;

import java.io.IOException;

import static org.fest.assertions.Assertions.assertThat;
import static play.test.Helpers.contentAsString;

/**
 * Simple (JUnit) tests that can call all parts of a play app.
 * If you are interested in mocking a whole application, see the wiki for more details.
 */
public class ApplicationServicesTest extends BaseApplicationServicesTest {

    @Before
    public void startPlay() {
        app = provideApplication();
        Helpers.start(app);
        mat = app.getWrappedApplication().materializer();
    }

    @After
    public void stopPlay() {
        if (app != null) {
            Helpers.stop(app);
            app = null;
        }
    }

    @Test
    @Override
    public void testGetAplication() throws IOException, ProcessingException {
        super.testGetAplication();
    }

    protected Application app;

    /**
     * The application's Akka streams Materializer.
     */
    protected Materializer mat;

    /**
     * Override this method to setup the application to use.
     *
     * By default this will call the old {@link #provideFakeApplication() provideFakeApplication} method.
     *
     * @return The application to use
     */
    protected Application provideApplication() {
        return provideFakeApplication();
    }

    /**
     *
     * Override this method to setup the fake application to use.
     *
     * @deprecated use the new {@link #provideApplication() provideApplication} method instead.
     *
     * @return The fake application to use
     */
    @Deprecated
    protected Application provideFakeApplication() {
        return Helpers.fakeApplication();
    }

  @Override
    protected String getCreateResult(JsonNode jsonNode, int expectedCode) {
        return TestUtils.getCreateResult(jsonNode, expectedCode, this);
    }

    @Override
    protected String getUpdateResult(String applicationId, JsonNode jsonNode, int expectedCode) {
        return TestUtils.getUpdateResult(applicationId, jsonNode, expectedCode, this);
    }

    @Override
    protected String getApplicationExistResult(String applicationId, int expectedCode) {
        return TestUtils.getExistResult(applicationId, expectedCode, this);
    }

    @Override
    protected String getApplicationsResult( int expectedCode) {
        return TestUtils.getGetSelectAllResult(expectedCode, this);
    }

    @Override
    protected String getGetApplicationResult(String applicationId, int expectedCode) {
        return TestUtils.getGetSelectResult(applicationId, expectedCode, this);
    }

    @Override
    protected String getDeleteMetricResult(String applicationId, int expectedCode) {
        return TestUtils.getDeleteResult(applicationId, expectedCode, this);
    }

}
