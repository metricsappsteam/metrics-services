import akka.stream.Materializer;
import com.fasterxml.jackson.databind.JsonNode;
import org.junit.After;
import org.junit.Before;
import play.Application;
import play.mvc.Http;
import play.mvc.Result;
import play.test.Helpers;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static play.test.Helpers.*;


/**
 * Simple (JUnit) tests that can call all parts of a play app.
 * If you are interested in mocking a whole application, see the wiki for more details.
 */
public class MetricsMetricsServicesTest extends BaseMetricsServicesTest /*extends WithApplication */{


    @Before
    public void startPlay() {
        app = provideApplication();
        Helpers.start(app);
        mat = app.getWrappedApplication().materializer();
    }

    @After
    public void stopPlay() {
        if (app != null) {
            Helpers.stop(app);
            app = null;
        }
    }

    protected Application app;

    /**
     * The application's Akka streams Materializer.
     */
    protected Materializer mat;

    /**
     * Override this method to setup the application to use.
     *
     * By default this will call the old {@link #provideFakeApplication() provideFakeApplication} method.
     *
     * @return The application to use
     */
    protected Application provideApplication() {
        return provideFakeApplication();
    }

    /**
     *
     * Override this method to setup the fake application to use.
     *
     * @deprecated use the new {@link #provideApplication() provideApplication} method instead.
     *
     * @return The fake application to use
     */
    @Deprecated
    protected Application provideFakeApplication() {
        return Helpers.fakeApplication();
    }


    @Override
    protected String getCreateResult(String applicationId, JsonNode jsonNode, int expectedCode) {
       /* Http.RequestBuilder requestBuilder = Helpers.fakeRequest("POST", BASE_CREATE + applicationId).bodyJson(jsonNode);
        Result result = Helpers.route(requestBuilder);
        assertThat(result.contentType().get()).isEqualTo("application/json");
        assertThat(result.status()).isEqualTo(expectedCode);
        String content = contentAsString(result);
        return content;*/
       return TestUtils.getCreateResult(applicationId, jsonNode, expectedCode, this);
    }

    @Override
    protected String getUpdateResult(String applicationId, JsonNode jsonNode, int expectedCode) {
       /* Http.RequestBuilder requestBuilder = Helpers.fakeRequest("POST", BASE_UPDATE + applicationId).bodyJson(jsonNode);
        Result result = Helpers.route(requestBuilder);
        assertThat(result.contentType().get()).isEqualTo("application/json");
        assertThat(result.status()).isEqualTo(expectedCode);
        String content = contentAsString(result);
        return content;*/
        return TestUtils.getUpdateResult(applicationId, jsonNode, expectedCode, this);
    }

    @Override
    protected String getMetricsExistResult(String applicationId, int expectedCode) {
       /* Http.RequestBuilder requestBuilder = Helpers.fakeRequest("GET", BASE_EXIST + applicationId);
        Result result = Helpers.route(requestBuilder);
        assertThat(result.contentType().get()).isEqualTo("application/json");
        assertThat(result.status()).isEqualTo(expectedCode);
        String content = contentAsString(result);
        return content;*/
       return TestUtils.getExistResult(applicationId, expectedCode, this);
    }


    @Override
    protected String getGetMetricResult(String applicationId, int expectedCode) {
       /* Http.RequestBuilder requestBuilder = Helpers.fakeRequest("GET", BASE_SELECT + applicationId);
        Result result = Helpers.route(requestBuilder);
        assertThat(result.contentType().get()).isEqualTo("application/json");
        assertThat(result.status()).isEqualTo(expectedCode);
        String content = contentAsString(result);
        return content;*/
        return TestUtils.getGetSelectResult(applicationId, expectedCode, this);

    }

    @Override
    protected String getDeleteMetricResult(String applicationId, int expectedCode) {
      /*  Http.RequestBuilder requestBuilder = Helpers.fakeRequest("POST", BASE_DELETE+ applicationId);
        Result result = Helpers.route(requestBuilder);
        assertThat(result.contentType().get()).isEqualTo("application/json");
        assertThat(result.status()).isEqualTo(expectedCode);
        String content = contentAsString(result);
        return content;*/
        return TestUtils.getDeleteResult(applicationId, expectedCode, this);

    }

}
