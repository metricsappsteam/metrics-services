import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import es.kibu.geoapis.metrics.MetricUtils;
import es.kibu.geoapis.objectmodel.MetricsDefinition;
import es.kibu.geoapis.serialization.MetricDefinitionWriter;
import es.kibu.geoapis.serialization.MetricsDefinitionReader;
import es.kibu.geoapis.services.objectmodel.results.*;
import org.joda.time.DateTime;
import org.junit.Test;
import play.Logger;
import play.libs.Json;

import java.io.IOException;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by lrodr_000 on 04/01/2017.
 */
public abstract class BaseMetricsServicesTest  extends BaseServicesTest{


    public static final String BASE_CREATE = "/api/v1/create-metrics/";
    public static final String BASE_UPDATE = "/api/v1/update-metrics/";
    public static final String BASE_DELETE = "/api/v1/delete-metrics/";
    public static final String BASE_SELECT = "/api/v1/metrics/";
    public static final String BASE_EXIST = "/api/v1/metrics-exist/";
    public static final String SAMPLE_APPID = "sample-appid";

    protected String getBaseDelete() {
        return BASE_DELETE;
    }

    @Override
    protected String getBaseCreate() {
        return BASE_CREATE;
    }

    @Override
    protected String getBaseUpdate() {
        return BASE_UPDATE;
    }

    @Override
    protected String getBaseExist() {
        return BASE_EXIST;
    }

    @Override
    protected String getBaseSelect() {
        return BASE_SELECT;
    }

    @Test
    public void testPreconditions() throws Exception {

        MetricsDefinition definition = MetricUtils.withMetricDefinition("resources/sample-metric-wikiloc-box-speed-geostats.json");
        assertThat(definition.getVariables().size()).isGreaterThan(0);

        MetricDefinitionWriter writer = new MetricDefinitionWriter();
        JsonNode jsonNode = Json.parse(writer.write(definition));

        String converted = jsonNode.toString();

        Logger.debug(converted);
        MetricsDefinitionReader reader = new MetricsDefinitionReader();
        MetricsDefinition definition1 = reader.readFrom(converted);

        assertEquals(definition.getVariables(), definition1.getVariables());

    }

    @Test
    public void testCreateMetric() throws IOException, ProcessingException {
        String applicationId = getRandomAppId();
        checkCreateMetric(applicationId, 200);
    }

    @Test
    public void creatingAMetricForTheSameAppTwiceShouldFail() throws IOException, ProcessingException {
        String applicationId = getRandomAppId();
        checkCreateMetric(applicationId, 200);
        checkCreateMetric(applicationId, 400);
    }

    private String getRandomAppId() {
        return String.format("sample-appid%s", Long.toString(DateTime.now().getMillis()));
    }

    @Test
    public void gettingAMetricForANonExistingAppShouldFail() throws IOException, ProcessingException {
        String applicationId = getRandomAppId();
        boolean isEmpty = checkGetMetric(applicationId, 400);
        assertTrue(isEmpty);
    }

    @Test
    public void updatingAMetricForNonExistignAppShouldFail() throws IOException, ProcessingException {
        String applicationId = getRandomAppId();
        checkUpdateForApp(applicationId, 400);
    }

    @Test
    public void testDelete() throws IOException, ProcessingException {
        String applicationId = getRandomAppId();
        checkDeleteMetric(applicationId, 200, "delete_failed");
    }

    @Test
    public void testDeleteComplex() throws IOException, ProcessingException {
        String applicationId = getRandomAppId();
        checkCreateMetric(applicationId, 200);
        checkDeleteMetric(applicationId, 200, "deleted");
    }

    @Test
    public void testUpdateMetric() throws IOException, ProcessingException {

        String applicationId = SAMPLE_APPID;
        checkUpdateForApp(applicationId, 200);

    }

    @Test
    public void testUpdateMetricAndDelete() throws IOException, Exception {

        String applicationId = getRandomAppId();
        try {
            //Log.d("CREATING_METRIC", "Creating metric for app: " + applicationId);
            checkCreateMetric(applicationId, 200);

            checkUpdateForApp(applicationId, 200);

        }finally {
            checkDeleteMetric(applicationId, 200, "deleted");
        }

    }

    @Test
    public void testMetricExists() throws IOException, ProcessingException {

        String applicationId = "sample-appid";
        checkCreateMetric(applicationId, 200);
        checkMetricsExist(applicationId);
    }

    @Test
    public void testGetMetric() throws IOException, ProcessingException {
        //String applicationId = "sample-appid";

        String applicationId = getRandomAppId();
        try {
            //Log.d("CREATING_METRIC", "Creating metric for app: " + applicationId);
            checkCreateMetric(applicationId, 200);
            checkGetMetric(applicationId, 200);

        }finally {
            checkDeleteMetric(applicationId, 200, "deleted");
        }
    }


    private void checkMetricsExist(String applicationId) throws ProcessingException, IOException {

        String content = getMetricsExistResult(applicationId, 200);
        ResultValue<ExistResult> existResult = ResultValueReader.asExistResult(content);

        assertTrue(existResult.getResult().isResult());
        assertTrue(existResult.getResult().getHash() != null);
        assertTrue(!existResult.getResult().getHash().isEmpty());

    }

    private boolean checkGetMetric(String applicationId, int expectedCode) {

        String content = getGetMetricResult(applicationId, expectedCode);
        JsonNode parse = Json.parse(content);

        if (expectedCode == 200) {
            assertTrue(parse.has("result"));
            assertTrue(/*parse.get("result").asText().equalsIgnoreCase("empty") || */ parse.get("result").has("content"));

            if (!parse.get("result").isTextual() && parse.get("result").has("content")) {
                assertTrue(parse.get("result").has("hash"));
                assertTrue(parse.get("result").has("isPublic"));
                assertTrue(parse.get("result").has("metricId"));
            }
        }
        else {
            return parse.get("errorMessage").isTextual() && parse.get("errorMessage").asText().contains("No metrics for app") /*parse.get("result").isTextual() && parse.get("result").asText().equalsIgnoreCase("empty")*/;
        }

        return false;
    }

    private void checkDeleteMetric(String applicationId, int expectedCode, String resultDeleted) throws ProcessingException, IOException {

        String content = getDeleteMetricResult(applicationId, expectedCode);
        ;

        JsonNode parse = Json.parse(content);

        if (expectedCode == 200) {
            ResultValue<DeleteResult> deleteResultResultValue = ResultValueReader.asDeleteResult(content);
            assertTrue(deleteResultResultValue.getResult().getResult().equalsIgnoreCase(resultDeleted));

        } else {
            assertTrue(parse.has("error") && parse.get("error").asBoolean());
        }
    }

    private void checkUpdateForApp(String applicationId, int expectedCode) throws ProcessingException, IOException {
        MetricsDefinition definition = MetricUtils.withMetricDefinition("resources/sample-metric-wikiloc-box-speed-geostats.json");
        MetricDefinitionWriter writer = new MetricDefinitionWriter();
        JsonNode jsonNode = Json.parse(writer.write(definition));

        String content = getUpdateResult(applicationId, jsonNode, expectedCode);

        JsonNode parse = Json.parse(content);

        if (expectedCode == 200) {
            ResultValue<UpdateResult> deleteResultResultValue = ResultValueReader.asUpdateResult(content);
            assertTrue(deleteResultResultValue.getResult().getResult().equalsIgnoreCase("updated"));
        } else {
            assertTrue(parse.get("error").asBoolean());
        }
    }

    private void checkCreateMetric(String applicationId, int expectedCode) throws ProcessingException, IOException {
        MetricsDefinition definition = MetricUtils.withMetricDefinition("resources/sample-metric-wikiloc-box-speed-geostats.json");
        MetricDefinitionWriter writer = new MetricDefinitionWriter();
        JsonNode jsonNode = Json.parse(writer.write(definition));

        String content = getCreateResult(applicationId, jsonNode, expectedCode);

        JsonNode parse = Json.parse(content);
        if (expectedCode == 200) {
            ResultValue<CreateResult> resultValue = ResultValueReader.asCreateResult(content);
            assertTrue(resultValue.getResult().getResult().equalsIgnoreCase("created"));
            assertTrue(resultValue.getResult().getId() != null);
            assertTrue(!resultValue.getResult().getId().isEmpty());

        } else {
            assertTrue(parse.has("error") && parse.get("error").asBoolean());
        }

    }

    protected abstract String getCreateResult(String applicationId, JsonNode jsonNode, int expectedCode);

    protected abstract String getUpdateResult(String applicationId, JsonNode jsonNode, int expectedCode);

    protected abstract String getMetricsExistResult(String applicationId, int expectedCode);

    protected abstract String getGetMetricResult(String applicationId, int expectedCode);

    protected abstract String getDeleteMetricResult(String applicationId, int expectedCode);
}
