import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import es.kibu.geoapis.services.objectmodel.results.Application;
import es.kibu.geoapis.services.objectmodel.results.CreateResult;
import es.kibu.geoapis.services.objectmodel.results.ResultValue;
import es.kibu.geoapis.services.objectmodel.results.ResultValueReader;
import org.joda.time.DateTime;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import play.test.Helpers;

import java.io.IOException;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertTrue;
import static play.test.Helpers.contentAsString;

/**
 * Created by lrodr_000 on 16/01/2017.
 */
public class TestUtils {

    public static Application getSampleApplication() {
        Application application = new Application();
        String time = DateTime.now().toDateTimeISO().toString();
        application.setName("Sample app at " + time);
        application.setDescription("Sample app description at " + time);
        return application;
    }

    public static String withCreatedApp() throws ProcessingException, IOException {

        int expectedCode = 200;

        JsonNode jsonNode = Json.toJson(TestUtils.getSampleApplication());

        BaseApplicationServicesTest.StaticApp  appServices = new BaseApplicationServicesTest.StaticApp();

        String content = TestUtils.getCreateResult(jsonNode, expectedCode, appServices);

        JsonNode parse = Json.parse(content);
        if (expectedCode == 200) {
            ResultValue<CreateResult> resultValue = ResultValueReader.asCreateResult(content);
            return resultValue.getResult().getId();
        } else {
            assertTrue(parse.has("error") && parse.get("error").asBoolean());
            return "";
        }

    }




    public static Application getSampleApplicationModified() {
        Application application = new Application();
        application.setName("Sample app <modified>");
        application.setDescription("Sample app description <modified>");
        return application;
    }
    
     public static String getCreateResult(JsonNode jsonNode, int expectedCode, BaseServicesTest test) {
        Http.RequestBuilder requestBuilder = Helpers.fakeRequest("POST", test.getBaseCreate()).bodyJson(jsonNode);
        Result result = Helpers.route(requestBuilder);
        assertThat(result.contentType().get()).isEqualTo("application/json");
        assertThat(result.status()).isEqualTo(expectedCode);
        String content = contentAsString(result);
        return content;
    }

    public static String getCreateResult(String applicationId, JsonNode jsonNode, int expectedCode, BaseServicesTest test) {
        Http.RequestBuilder requestBuilder = Helpers.fakeRequest("POST", test.getBaseCreate() + applicationId).bodyJson(jsonNode);
        Result result = Helpers.route(requestBuilder);
        String content = contentAsString(result);
        assertThat(result.contentType().get()).isEqualTo("application/json");
        assertThat(result.status()).isEqualTo(expectedCode);
        return content;
    }

    public static String getUpdateResult(String applicationId, JsonNode jsonNode, int expectedCode, BaseServicesTest test) {
        Http.RequestBuilder requestBuilder = Helpers.fakeRequest("POST", test.getBaseUpdate() + applicationId).bodyJson(jsonNode);
        Result result = Helpers.route(requestBuilder);
        assertThat(result.contentType().get()).isEqualTo("application/json");
        assertThat(result.status()).isEqualTo(expectedCode);
        String content = contentAsString(result);
        return content;
    }


    public static String getExistResult(String applicationId, int expectedCode, BaseServicesTest test) {
        Http.RequestBuilder requestBuilder = Helpers.fakeRequest("GET", test.getBaseExist() + applicationId);
        Result result = Helpers.route(requestBuilder);
        assertThat(result.contentType().get()).isEqualTo("application/json");
        assertThat(result.status()).isEqualTo(expectedCode);
        String content = contentAsString(result);
        return content;
    }



    public static String getGetSelectResult(String applicationId, int expectedCode, BaseServicesTest test) {
        Http.RequestBuilder requestBuilder = Helpers.fakeRequest("GET", test.getBaseSelect() + applicationId);
        Result result = Helpers.route(requestBuilder);
        assertThat(result.contentType().get()).isEqualTo("application/json");
        assertThat(result.status()).isEqualTo(expectedCode);
        String content = contentAsString(result);
        return content;
    }

    public static String getGetSelectAllResult(int expectedCode, BaseServicesTest test) {
        Http.RequestBuilder requestBuilder = Helpers.fakeRequest("GET", test.getBaseList());
        Result result = Helpers.route(requestBuilder);
        assertThat(result.contentType().get()).isEqualTo("application/json");
        assertThat(result.status()).isEqualTo(expectedCode);
        String content = contentAsString(result);
        return content;
    }

    public static String getDeleteResult(String applicationId, int expectedCode, BaseServicesTest test) {
        Http.RequestBuilder requestBuilder = Helpers.fakeRequest("POST", test.getBaseDelete() + applicationId);
        Result result = Helpers.route(requestBuilder);
        assertThat(result.contentType().get()).isEqualTo("application/json");
        assertThat(result.status()).isEqualTo(expectedCode);
        String content = contentAsString(result);
        return content;
    }


}
