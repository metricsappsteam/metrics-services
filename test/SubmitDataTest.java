import akka.stream.Materializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import controllers.MetricsServices;
import es.kibu.geoapis.metrics.MetricUtils;
import es.kibu.geoapis.objectmodel.MetricsDefinition;
import es.kibu.geoapis.serialization.BasicMetricsDefinitionWriter;
import es.kibu.geoapis.services.MetricsStorageUtils;
import models.persistence.MetricsPersistence;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import play.Application;
import play.Logger;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import play.test.Helpers;

import java.io.IOException;
import java.util.Optional;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static play.test.Helpers.contentAsString;

/**
 * Created by lrodriguez2002cu on 05/01/2017.
 */

@RunWith(PowerMockRunner.class)
@PrepareForTest(MetricsPersistence.class)
@PowerMockIgnore("javax.management.*")
public class SubmitDataTest {


    @Mock
    MetricsServices metricsServicesController;

    @Before
    public void startPlay() {

        //metricsServicesController = mock(controllers.MetricsServices.class);
        PowerMockito.mockStatic(MetricsPersistence.class);

        app = provideApplication();

        Helpers.start(app);
        mat = app.getWrappedApplication().materializer();
    }

    @After
    public void stopPlay() {
        if (app != null) {
            Helpers.stop(app);
            app = null;
        }
    }

    protected Application app;

    /**
     * The application's Akka streams Materializer.
     */
    protected Materializer mat;


    /**
     * Override this method to setup the application to use.
     * <p>
     * By default this will call the old {@link #provideFakeApplication() provideFakeApplication} method.
     *
     * @return The application to use
     */
    protected Application provideApplication() {
        return provideFakeApplication();
    }


    /**
     * Override this method to setup the fake application to use.
     *
     * @return The fake application to use
     * @deprecated use the new {@link #provideApplication() provideApplication} method instead.
     */
    @Deprecated
    protected Application provideFakeApplication() {
        return Helpers.fakeApplication();
    }


    /* private CompletionStage<Done> getFakeCompletionStage(String variable, String appid, String content ){
         Optional<MetricsStorageUtils.MetricsDef> metricsDef = getMetricsDef(appid);
         final Materializer materializer = ActorMaterializer.create(system());
         final String topicName = getKey(variable, appid);
         CompletionStage<Done> done = getSubmitCompletionStage1(materializer, content, topicName);
         return done;
     }
 */
    @Test
    //@Ignore
    public void testSubmitDataDryRun() throws IOException, ProcessingException {
        // Create and train mock

        MetricsDefinition definition = MetricUtils.withMetricDefinition("resources/sample-metric-wikiloc-box-speed-geostats.json");
        BasicMetricsDefinitionWriter writer = new BasicMetricsDefinitionWriter();
        String defStr = writer.write(definition);

        String application = "app1";
        String session = "session";
        String user = "user";
        String variable = "movement";

        Optional<MetricsStorageUtils.MetricsDef> madeupDefinition = Optional.of(new MetricsStorageUtils.MetricsDef(defStr, "madeuphash", definition.getId(), true));


        when(metricsServicesController.testing()).thenReturn(true);
        Mockito.when(MetricsPersistence.getMetricsDef(application)).thenReturn(madeupDefinition);

        //CompletionStage<Done> fakeCompletionStage = getFakeCompletionStage(variable, application, jsonNode.toString());


        JsonNode jsonNode = getDataJson(application, session, user, 200);
        // check value
        //assertEquals("first", mockedList.get(0));

        // verify interaction
        //verify(mockedList).get(0);

        getSubmitDataResult(application, variable, jsonNode, 200, true);

    }

    @Test
    //@Ignore
    public void testSubmitData() throws IOException, ProcessingException {
        // Create and train mock

        MetricsDefinition definition = MetricUtils.withMetricDefinition("resources/sample-metric-wikiloc-box-speed-geostats.json");
        BasicMetricsDefinitionWriter writer = new BasicMetricsDefinitionWriter();
        String defStr = writer.write(definition);

        String application = "app1";
        String session = "session";
        String user = "user";
        String variable = "movement";

        Optional<MetricsStorageUtils.MetricsDef> madeupDefinition = Optional.of(new MetricsStorageUtils.MetricsDef(defStr, "madeuphash", definition.getId(), true));


        when(metricsServicesController.testing()).thenReturn(true);
        Mockito.when(MetricsPersistence.getMetricsDef(application)).thenReturn(madeupDefinition);

        //CompletionStage<Done> fakeCompletionStage = getFakeCompletionStage(variable, application, jsonNode.toString());


        JsonNode jsonNode = getDataJson(application, session, user, 200);
        // check value
        //assertEquals("first", mockedList.get(0));

        // verify interaction
        //verify(mockedList).get(0);

        getSubmitDataResult(application, variable, jsonNode, 200, false);

    }


    @Test
    //@Ignore
    public void testSubmitDataAndCreate() throws IOException, ProcessingException {
        // Create and train mock

        int times = 1000;
        MetricsDefinition definition = MetricUtils.withMetricDefinition("resources/sample-metric-wikiloc-box-speed-geostats.json");
        BasicMetricsDefinitionWriter writer = new BasicMetricsDefinitionWriter();
        String defStr = writer.write(definition);

        String application = "app1";
        String session = "session";
        String user = "user";
        String variable = "movement";

        Optional<MetricsStorageUtils.MetricsDef> madeupDefinition = Optional.of(new MetricsStorageUtils.MetricsDef(defStr, "madeuphash", definition.getId(), true));

        when(metricsServicesController.testing()).thenReturn(true);
        Mockito.when(MetricsPersistence.getMetricsDef(application)).thenReturn(madeupDefinition);

        //CompletionStage<Done> fakeCompletionStage = getFakeCompletionStage(variable, application, jsonNode.toString());

        while (times-- >0) {
            JsonNode jsonNode = getDataJson(application, session, user, 200);
            // check value
            //assertEquals("first", mockedList.get(0));
            // verify interaction
            //verify(mockedList).get(0);
            getSubmitDataResult(application, variable, jsonNode, 200, false);
        }
    }


    // POST        /api/v1/submit-data/:appid/:variable        controllers.MetricsServices.submitData(appid: String, variable: String)

    public static final String BASE_SUBMIT = "/api/v1/submit-data/";


    public static final String testBodyTemplate =
            "{\"records\":[\n" +
                    "\t{\n" +
                    "\t\"key\": \"%s\" ,\n" +
                    "\t\"value\": {\n" +
                    "\t\t\"application\": \"%s\",\n" +
                    "\t\t\"user\": \"%s\",\n" +
                    "\t\t\"session\": \"%s\"\n" +
                    "\t\t\"steps\": %s\n" +
                    "\t\t}\n" +
                    "\t}\n" +
                    "]\n" +
                    "\t\n" +
                    "}";


    /*
     {
	"application": "%s",
    "user": "%s",
    "session": "%s",
    "steps": %s,
    "location": {
    "lat": %s,
    "lon": %s
    }
}
     */
    public static final String testBodySimpleTemplate =
            " {\n" +
                    "\t\"application\": \"%s\",\n" +
                    "    \"user\": \"%s\",\n" +
                    "    \"session\": \"%s\",\n" +
                    "    \"steps\": %s, \n" +
                    "    \"location\": {\n" +
                    "    \"lat\": %s,\n" +
                    "    \"lon\": %s   \n" +
                    "    }\n" +
                    "}\n";

    public String getData(String application, String session, String user, int steps) {
        double lat = 39.0455;
        double lon = 0.0455;
        return String.format(testBodySimpleTemplate, application, session, user, Integer.toString(steps), Double.toString(lat), Double.toString(lon));
    }

    public JsonNode getDataJson(String application, String session, String user, int steps) {
        String data = getData(application, session, user, steps);
        return Json.parse(data);
    }


    protected String getSubmitDataResult(String applicationId, String variable, JsonNode metricsData, int expectedCode, boolean dryrun) {

        Logger.debug(metricsData.toString());

        String dryRunPart = (dryrun) ? "?dryrun=true" : "";
        String uri = BASE_SUBMIT + applicationId + "/" + variable + dryRunPart;
        Logger.debug("URI: {}", uri);
        Http.RequestBuilder requestBuilder = Helpers.fakeRequest("POST", uri).bodyJson(metricsData);

        Result result = Helpers.route(requestBuilder);
        String content = contentAsString(result);
        Logger.debug("Content: {}", content);
        assertThat(result.contentType().get()).isEqualTo("application/json");
        assertThat(result.status()).isEqualTo(expectedCode);
        return content;
    }
}
