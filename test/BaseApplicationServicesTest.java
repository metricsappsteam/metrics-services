import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import es.kibu.geoapis.services.objectmodel.results.*;
import org.joda.time.DateTime;
import org.junit.Test;
import play.Logger;
import play.libs.Json;

import java.io.IOException;
import java.util.Iterator;

import static org.junit.Assert.assertTrue;

/**
 * Created by lrodr_000 on 04/01/2017.
 */
public abstract class BaseApplicationServicesTest extends BaseServicesTest{
/*
    GET         /api/v1/applications/:appid                      controllers.ApplicationServices.getApplication(appid: String)
    GET         /api/v1/application-exist/:appid                controllers.ApplicationServices.applicationExists(appid: String)

    POST        /api/v1/update-application/:appid               controllers.ApplicationServices.updateApplication(appid: String)
    POST        /api/v1/create-application                      controllers.ApplicationServices.createApplication()
    POST        /api/v1/delete-application/:appid               controllers.ApplicationServices.deleteApplication(appid: String)*/

    public static final String BASE_CREATE = "/api/v1/create-application/";
    public static final String BASE_UPDATE = "/api/v1/update-application/";
    public static final String BASE_DELETE = "/api/v1/delete-application/";
    public static final String BASE_SELECT = "/api/v1/applications/";
    public static final String BASE_EXIST =  "/api/v1/application-exist/";

    @Test
    public void testCreateApplication() throws IOException, ProcessingException {
        //String applicationId = getRandomAppId();
        String applicationId = checkCreateApplication(200);
        Logger.debug(String.format("Created application with id: %s", applicationId));

    }

   /* @Test
    public void creatingAMetricForTheSameAppTwiceShouldFail() throws IOException, ProcessingException {
        String applicationId = getRandomAppId();
        checkCreateApplication(applicationId, 200);
        checkCreateApplication(applicationId, 400);
    }*/

    private String getRandomAppId() {
        return String.format("sample-appid%s", Long.toString(DateTime.now().getMillis()));
    }

    @Test
    public void gettingANonExistingAppShouldFail() throws IOException, ProcessingException {
        String applicationId = getRandomAppId();
        boolean isEmpty = checkGetApplication(applicationId, 400);
        assertTrue(isEmpty);
    }

    @Test
    public void updatingAMetricForNonExistingAppShouldFail() throws IOException, ProcessingException {
        String applicationId = getRandomAppId();
        checkUpdateForApp(applicationId, 400);
    }

    @Test
    public void testDelete() throws IOException, ProcessingException {
        String applicationId = getRandomAppId();
        checkDeleteApplication(applicationId, 200, "delete_failed");
    }

    @Test
    public void testDeleteComplex() throws IOException, ProcessingException {
        //String applicationId = getRandomAppId();
        String applicationId = checkCreateApplication(200);
        checkDeleteApplication(applicationId, 200, "deleted");
    }

    @Test
    public void testUpdateApplication() throws IOException, ProcessingException {

        String applicationId = checkCreateApplication(200);
        checkUpdateForApp(applicationId, 200);

    }

    @Test
    public void testUpdateApplicationAndDelete() throws IOException, Exception {

        String applicationId = null;
        try {
            //Log.d("CREATING_METRIC", "Creating metric for app: " + applicationId);
            applicationId = checkCreateApplication(200);

            checkUpdateForApp(applicationId, 200);

        } finally {
            checkDeleteApplication(applicationId, 200, "deleted");
        }

    }

    @Test
    public void testApplicationExist() throws IOException, ProcessingException {
        String applicationId = checkCreateApplication(200);
        checkApplicationsExist(applicationId);
    }

    @Test
    public void testGetAplication() throws IOException, ProcessingException {
        String applicationId = null;
        try {
            applicationId = checkCreateApplication(200);
            checkGetApplication(applicationId, 200);

        } finally {
            checkDeleteApplication(applicationId, 200, "deleted");
        }
    }

    @Test
    public void testGetAplications() throws IOException, ProcessingException {
        String applicationId = null;
        try {
            applicationId = checkCreateApplication(200);
            //at least one application
            checkGetApplications(200);

        } finally {
            checkDeleteApplication(applicationId, 200, "deleted");
        }
    }


    private void checkApplicationsExist(String applicationId) throws ProcessingException, IOException {

        String content = getApplicationExistResult(applicationId, 200);
        ResultValue<ExistResult> existResult = ResultValueReader.asExistResult(content);
        assertTrue(existResult.getResult().isResult());
        //ssertTrue(existResult.getResult().getHash() != null);
        //assertTrue(!existResult.getResult().getHash().isEmpty());

    }

    private boolean checkGetApplication(String applicationId, int expectedCode) {

        String content = getGetApplicationResult(applicationId, expectedCode);
        JsonNode parse = Json.parse(content);

        if (expectedCode == 200) {
            assertTrue(parse.has("result"));

            if (!parse.get("result").isTextual() /*&& parse.get("result").has("content")*/) {
                assertTrue(parse.get("result").has("appid"));
                assertTrue(parse.get("result").has("name"));
                assertTrue(parse.get("result").has("description"));
            }
        } else {
            return parse.get("errorMessage").isTextual() && parse.get("errorMessage").asText().contains("No metrics for app") /*parse.get("result").isTextual() && parse.get("result").asText().equalsIgnoreCase("empty")*/;
        }

        return false;

    }



    private boolean checkGetApplications(int expectedCode) {

        String content = getApplicationsResult(expectedCode);
        JsonNode parse = Json.parse(content);

        if (expectedCode == 200) {
            assertTrue(parse.has("result"));

            if (!parse.get("result").isTextual() /*&& parse.get("result").has("content")*/) {
                assertTrue(parse.get("result").isArray());

                Iterator<JsonNode> result = parse.get("result").elements();
                while (result.hasNext()) {
                    JsonNode elem = result.next();
                    assertTrue(elem.has("appid"));
                    assertTrue(elem.has("name"));
                    assertTrue(elem.has("description"));
                }

            }
        } else {
            return parse.get("errorMessage").isTextual() && parse.get("errorMessage").asText().contains("No metrics for app") /*parse.get("result").isTextual() && parse.get("result").asText().equalsIgnoreCase("empty")*/;
        }

        return false;
    }


    private void checkDeleteApplication(String applicationId, int expectedCode, String resultDeleted) throws ProcessingException, IOException {

        String content = getDeleteMetricResult(applicationId, expectedCode);

        JsonNode parse = Json.parse(content);

        if (expectedCode == 200) {
            ResultValue<DeleteResult> deleteResultResultValue = ResultValueReader.asDeleteResult(content);
            assertTrue(deleteResultResultValue.getResult().getResult().equalsIgnoreCase(resultDeleted));

        } else {
            assertTrue(parse.has("error") && parse.get("error").asBoolean());
        }
    }

    private void checkUpdateForApp(String applicationId, int expectedCode) throws ProcessingException, IOException {
        JsonNode jsonNode = Json.toJson(TestUtils.getSampleApplicationModified());

        String content = getUpdateResult(applicationId, jsonNode, expectedCode);

        JsonNode parse = Json.parse(content);

        if (expectedCode == 200) {
            ResultValue<UpdateResult> deleteResultResultValue = ResultValueReader.asUpdateResult(content);
            assertTrue(deleteResultResultValue.getResult().getResult().equalsIgnoreCase("updated"));
        } else {
            assertTrue(parse.get("error").asBoolean());
        }
    }



    private String checkCreateApplication(int expectedCode) throws ProcessingException, IOException {

        JsonNode jsonNode = Json.toJson(TestUtils.getSampleApplication());

        String content = getCreateResult(jsonNode, expectedCode);

        JsonNode parse = Json.parse(content);
        if (expectedCode == 200) {
            ResultValue<CreateResult> resultValue = ResultValueReader.asCreateResult(content);
            assertTrue(resultValue.getResult().getResult().equalsIgnoreCase("created"));
            assertTrue(resultValue.getResult().getId() != null);
            assertTrue(!resultValue.getResult().getId().isEmpty());
            return resultValue.getResult().getId();

        } else {
            assertTrue(parse.has("error") && parse.get("error").asBoolean());
            return "";
        }

    }

    protected abstract String getCreateResult(JsonNode jsonNode, int expectedCode);

    protected abstract String getUpdateResult(String applicationId, JsonNode jsonNode, int expectedCode);

    protected abstract String getApplicationExistResult(String applicationId, int expectedCode);

    protected abstract String getGetApplicationResult(String applicationId, int expectedCode);

    protected abstract String getDeleteMetricResult(String applicationId, int expectedCode);

    protected abstract String  getApplicationsResult(int expectedCode);

    protected String getBaseSelect() {
        return BASE_SELECT;
    }

    protected String getBaseDelete() {
        return BASE_DELETE;
    }

    @Override
    protected String getBaseCreate() {
        return BASE_CREATE;
    }

    @Override
    protected String getBaseUpdate() {
        return BASE_UPDATE;
    }

    @Override
    protected String getBaseExist() {
        return BASE_EXIST;
    }


    public static class StaticApp extends BaseServicesTest{

        public String getBaseSelect() {
            return BASE_SELECT;
        }

        public String getBaseDelete() {
            return BASE_DELETE;
        }

        @Override
        public String getBaseCreate() {
            return BASE_CREATE;
        }

        @Override
        public String getBaseUpdate() {
            return BASE_UPDATE;
        }

        @Override
        public String getBaseExist() {
            return BASE_EXIST;
        }

        @Override
        protected String getBaseList() {
            return BASE_SELECT;
        }
    }



}
