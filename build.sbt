name := "metrics-services"

version := "1.0.23"

lazy val `metrics-services` = (project in file(".")).enablePlugins(PlayJava).enablePlugins(DockerPlugin).enablePlugins(BuildInfoPlugin).
  settings(
    buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion, sbtVersion),
    buildInfoPackage := "services"
  )

buildInfoOptions += BuildInfoOption.ToJson

//buildInfoOptions += BuildInfoOption.BuildTime

scalaVersion := "2.11.8"

libraryDependencies ++= Seq( javaJdbc ,  cache , javaWs )

libraryDependencies += "es.kibu.geoapis" % "metrics-common" % "1.0-SNAPSHOT"

libraryDependencies += "es.kibu.geoapis" % "metrics-server-common" % "1.0-SNAPSHOT"

libraryDependencies += "es.kibu.geoapis.backend" % "cassandra-persistence-actor" % "1.0-SNAPSHOT"

libraryDependencies += "es.kibu.geoapis" % "metrics-core" % "1.0-SNAPSHOT"

libraryDependencies += "es.kibu.geoapis.backend" % "actors" % "1.0-SNAPSHOT" exclude("org.slf4j", "slf4j-log4j12")

// https://mvnrepository.com/artifact/org.easytesting/fest-assert
libraryDependencies += "org.easytesting" % "fest-assert" % "1.4"

libraryDependencies += "com.typesafe.akka" %% "akka-stream-kafka" % "0.13"

// https://mvnrepository.com/artifact/junit/junit
libraryDependencies += "junit" % "junit" % "4.12" % "test"

libraryDependencies ++= Seq(
  "io.swagger" %% "swagger-play2" % "1.5.3"
)

libraryDependencies += "org.webjars" % "swagger-ui" % "2.2.10-1"

// https://mvnrepository.com/artifact/com.typesafe.akka/akka-slf4j_2.11
libraryDependencies += "com.typesafe.akka" % "akka-slf4j_2.11" % "2.3.6"

libraryDependencies ~= { _.map(_.exclude("org.slf4j", "slf4j-log4j12")) }

libraryDependencies ~= { _.map(_.exclude("org.slf4j", "slf4j-simple")) }

libraryDependencies ~= { _.map(_.exclude("com.datastax.spark", "spark-cassandra-connector_2.11")) }

//com.datastax.spark:spark-cassandra-connector_2.11:2.0.0

//exclude("org.slf4j", "slf4j-simple")
//libraryDependencies ~= { _.map(_.exclude("ch.qos.logback", "logback-classic")) }

// https://mvnrepository.com/artifact/org.mockito/mockito-all
libraryDependencies += "org.mockito" % "mockito-all" % "2.0.0-beta" % "test"

// https://mvnrepository.com/artifact/org.powermock/powermock-api-mockito
libraryDependencies += "org.powermock" % "powermock-api-mockito" % "1.6.6" % "test"

// https://mvnrepository.com/artifact/org.powermock/powermock-module-junit4
libraryDependencies += "org.powermock" % "powermock-module-junit4" % "1.6.6"  % "test"

libraryDependencies += filters

unmanagedResourceDirectories in Test <+=  baseDirectory ( _ /"target/web/public/test" )  

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"

resolvers += Resolver.mavenLocal

// (stage in Docker) <<= (stage in Docker).dependsOn(swagger)

//resolvers += "Local Maven" at "d:/installers/maven/.m2/repository"
maintainer in Docker := "Luis E. Rodriguez"

// it gives some error if enabled
// dockerUpdateLatest := true
// dockerUpdateLatest := true

dockerRepository := Some("reg.geotecuji.org")


packageOptions += Package.ManifestAttributes(
  "Implementation-Version" -> (version in ThisBuild).value,
  "Implementation-Title" -> name.value
)

