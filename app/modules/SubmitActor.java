package modules;

import akka.Done;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.UntypedActor;
import akka.japi.function.Function;
import akka.kafka.ProducerSettings;
import akka.kafka.javadsl.Producer;
import akka.stream.*;
import akka.stream.javadsl.RunnableGraph;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import akka.stream.javadsl.SourceQueueWithComplete;
import controllers.KafkaConstants;
import controllers.MetricsServices;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import play.Configuration;
import play.Logger;

import javax.inject.Inject;
import java.io.Serializable;
import java.util.concurrent.CompletionStage;

import static es.kibu.geoapis.metrics.actors.KafkaConsts.KAFKA_BOOTSTRAP_SERVERS;

public class SubmitActor extends UntypedActor {

    public static final String SUBMIT_ACTOR_NAME = "submit-actor";

    SourceQueueWithComplete<Object> queue;
    ProducerSettings producerSettings;

    @Inject
    Configuration config;
    ActorSystem system;

    final Materializer materializer;
    private Sink<ProducerRecord<String, String>, CompletionStage<Done>> kafkaSink;

    @Inject
    public SubmitActor(ActorSystem system, Configuration configuration) {
        this.config = configuration;
        this.system = system;
        ActorMaterializerSettings actorMaterializerSettings = ActorMaterializerSettings.apply(system).withSupervisionStrategy(new Function<Throwable, Supervision.Directive>() {
            @Override
            public Supervision.Directive apply(Throwable param) throws Exception {
                Logger.error("Error with the materializer actor", param);
                return Supervision.stop();
            }
        });

        materializer = ActorMaterializer.create(actorMaterializerSettings, system);
    }


    private ProducerSettings getProducerSettings() {
        if (producerSettings == null) {
            String bootstrapServers = config.getString(KAFKA_BOOTSTRAP_SERVERS);
            Logger.debug("boostrap server configured: {}", bootstrapServers);
            producerSettings = ProducerSettings
                    .create(system, new StringSerializer(), new StringSerializer())
                    .withBootstrapServers(bootstrapServers);
        }
        return producerSettings;
    }

    private Sink<ProducerRecord<String, String>, CompletionStage<Done>> createKafkaSink(boolean fake) {
        boolean artificial = fake || false;
        if (kafkaSink == null || artificial) {
            if (artificial) {
                kafkaSink = Sink.foreach(param -> {
                    throw new RuntimeException(String.format("Failed while receiving key: %s value: %s", param.key(), param.value()));
                    //Logger.debug(String.format("Sink received key: %s value: %s", param.key(), param.value()));
                });
            } else kafkaSink = Producer.plainSink(getProducerSettings());
        }
        return kafkaSink;
    }

    public static enum SubmitResult {SUBMITTED, FAILED}

    public static class SubmitResultData implements Serializable {
        SubmitResult result;
        String reason;

        public SubmitResult getResult() {
            return result;
        }

        public String getReason() {
            return reason;
        }

        public SubmitResultData(SubmitResult result, String reason) {
            this.result = result;
            this.reason = reason;
        }

        public static SubmitResultData ok() {
            return new SubmitResultData(SubmitResult.SUBMITTED, "");
        }

        public static SubmitResultData failed(String reason) {
            return new SubmitResultData(SubmitResult.FAILED, reason);
        }
    }

    @Override
    public void onReceive(Object message) throws Throwable {
        Logger.info("The sender outside is: " + sender());
        final ActorRef originalSender = sender();
        getQueue().offer(message)
                .handleAsync((offerResult, throwable) -> {

                    Logger.debug("The sender is: " + originalSender);
                    Logger.debug(String.format("offer result is: %s", offerResult != null ? offerResult.getClass().getSimpleName() : " null"));
                    boolean failed = (offerResult == null) || !offerResult.equals(QueueOfferResult.enqueued());
                    failed = failed || offerResult.equals(QueueOfferResult.dropped());

                    if (throwable != null || failed) {
                        if (throwable != null) {
                            Logger.error("Error while sending kafka producer record:", throwable);
                            //return badRequest(Json.toJson(resultObject(throwable)));
                            originalSender.tell(SubmitResultData.failed(throwable.getMessage()), self());

                        } else {
                            String msg = "Error while sending kafka producer record: data is not enqueued into the stream.";
                            Logger.error(msg);
                            originalSender.tell(SubmitResultData.failed(msg), self());
                        }

                    } else {
                        Logger.info("Data enqeued into the stream.");
                        originalSender.tell(SubmitResultData.ok(), this.getSelf());
                    }
                    return true;
                });
    }

    /*private ActorRef createSubmitActorRef(Materializer materializer) {
        RunnableGraph<ActorRef> runnableGraphToKafkaActor = runnableGraphToKafkaActor();
        ActorRef actor = runnableGraphToKafkaActor.run(materializer);
        return actor;
    }

    private RunnableGraph<ActorRef> runnableGraphToKafkaActor() {
        //new VariableData(topicName, content)
        RunnableGraph<ActorRef> to = Source.actorRef(0, OverflowStrategy.fail())
                .map(rawElem -> {
                    MetricsServices.VariableData elem = (MetricsServices.VariableData) rawElem;
                    Logger.debug("Sending data to topic: {}, key: {}", KafkaConstants.TOPIC, elem.getKey());
                    return new ProducerRecord<>(KafkaConstants.TOPIC, elem.getKey(), elem.getContent());
                })
                .to(createKafkaSink());
        return to; //.runWith(createKafkaSink(), materializer);
    }*/


    private RunnableGraph<SourceQueueWithComplete<Object>> runnableGraphToKafkaQueue() {
        //new VariableData(topicName, content)
        RunnableGraph<SourceQueueWithComplete<Object>> to = Source.queue(0, OverflowStrategy.dropNew())
                .map(rawElem -> {
                    MetricsServices.VariableData elem = (MetricsServices.VariableData) rawElem;
                    Logger.debug("Sending data to topic: {}, key: {}", KafkaConstants.TOPIC, elem.getKey());
                    return new ProducerRecord<>(KafkaConstants.TOPIC, elem.getKey(), elem.getContent());
                })
                .to(createKafkaSink());
        return to; //.runWith(createKafkaSink(), materializer);
    }

    private SourceQueueWithComplete<Object> getQueue() {
        return getQueue(materializer);
    }

    private SourceQueueWithComplete<Object> getQueue(Materializer materializer) {
        if (queue == null) {
            queue = createQueueSourceGraph(materializer);
        }
        return queue;
    }

    private SourceQueueWithComplete<Object> createQueueSourceGraph(Materializer materializer) {
        RunnableGraph<SourceQueueWithComplete<Object>> sourceQueueWithCompleteRunnableGraph = runnableGraphToKafkaQueue();
        SourceQueueWithComplete<Object> run = sourceQueueWithCompleteRunnableGraph.run(materializer);
        return run;
    }

    private Sink<ProducerRecord<String, String>, CompletionStage<Done>> createKafkaSink() {
        return createKafkaSink(false);
    }


}