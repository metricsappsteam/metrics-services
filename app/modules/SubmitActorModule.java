package modules;

import com.google.inject.AbstractModule;
import play.libs.akka.AkkaGuiceSupport;

import static modules.SubmitActor.SUBMIT_ACTOR_NAME;

public class SubmitActorModule extends AbstractModule implements AkkaGuiceSupport {
    @Override
    protected void configure() {
        bindActor(SubmitActor.class, SUBMIT_ACTOR_NAME);
    }
}
