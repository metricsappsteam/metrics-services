package controllers;

import akka.actor.ActorSystem;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import es.kibu.geoapis.backend.actors.messages.Identifiable;
import es.kibu.geoapis.backend.actors.persistence.PersistenceActor;
import es.kibu.geoapis.backend.actors.persistence.PersistenceMessageUtils;
import es.kibu.geoapis.services.ApplicationManager;
import es.kibu.geoapis.services.ExecutionParamsManager;
import es.kibu.geoapis.services.objectmodel.results.*;
import io.swagger.annotations.*;
import models.Utils;
import models.persistence.ApplicationPersistence;
import models.persistence.ExecutionParamsPersistence;
import models.persistence.MetricsPersistence;
import play.Logger;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.util.Optional;

import static es.kibu.geoapis.services.objectmodel.results.ResultValue.resultObject;

/**
 * Created by lrodr_000 on 16/01/2017.
 */
@Api(value = "ExecutionParams", description = "Operations related the configuration of some application parameters" +
        " (i.e. setting the server key for the push service or the push url)")
public class ExecutionParamsServices extends Controller {

    public static final String THE_APPLICATION_ID = "The application id";

    ActorSystem system;

    @Inject
    public ExecutionParamsServices(ActorSystem system) {
        this.system = system;
        ExecutionParamsPersistence.setSystem(system);
        ApplicationPersistence.setSystem(system);
        MetricsPersistence.setSystem(system);
    }


    @ApiOperation(value = "Creates the execution params",
            response = CreateResult.class)

    public Result createExecutionParams(String appId) {
        Logger.debug("Creating application");
        try {
            Object result = doCreate(appId);
            boolean wasOk = result instanceof Identifiable;// !PersistenceMessageUtils.isError((PersistenceActor.QueryResult) result);
            if (wasOk) {
                //String id = (String) ((Identifiable) result).getId();
                String id  =((ExecutionParamsManager.ExecutionParamsModel)result).getId();
                CreateResult created = new CreateResult("created", id);
                return ok(Json.toJson(resultObject(created)));
            } else {
                return badRequest(Json.toJson(resultObject(PersistenceMessageUtils.getErrorMsg((PersistenceActor.QueryResult) result))));
            }

        } catch (Exception e) {
            return getErrorResponse(e);
        }
    }

    private Result getErrorResponse(Exception e) {
        return Utils.getErrorResponse(e);
    }

    @ApiOperation(value = "Updates the configuration associated to the specified application",
            response = UpdateResult.class)
    public Result updateExecutionParams(@ApiParam(value = THE_APPLICATION_ID) String appid) {
        try {
            Object result = doUpdate(appid);
            boolean updated = !PersistenceMessageUtils.isError((PersistenceActor.QueryResult) result);
            UpdateResult updateResult = new UpdateResult(updated ? "updated" : "update_failed");
            return ok(Json.toJson(resultObject(updateResult)));
        } catch (Exception e) {
            return getErrorResponse(e);
        }
    }


    private Object doCreate(String appid) throws Exception{
        Utils.checkApplicationExist(system, appid);
        ExistResult existResult = ExecutionParamsPersistence.executionParamsExists(appid);
        if (!existResult.isResult()) {
            ExecutionParamsManager.ExecutionParamsModel executionParamsModel = getExecutionParamsModelFromBody();

            return ExecutionParamsPersistence.createExecutionParams(appid, executionParamsModel);
        } else {
            String message = String.format("Exec params already exist for application: %s", appid);
            throw new Utils.ServiceException(message);
        }
    }

    private ExecutionParamsManager.ExecutionParamsModel getExecutionParamsModelFromBody() throws IOException {
        JsonNode jsonNode = request().body().asJson();
        ObjectMapper mapper = new ObjectMapper();
        ExecutionParams executionParams = mapper.readValue(jsonNode.toString(), ExecutionParams.class);
        ExecutionParamsManager.ExecutionParamsModel executionParamsModel = new ExecutionParamsManager.ExecutionParamsModel();
        executionParamsModel.fromExecutionParams(executionParams);
        return executionParamsModel;
    }

    private Object doUpdate(String appid) throws ProcessingException, NoSuchAlgorithmException {
        ExistResult existResult = ExecutionParamsPersistence.executionParamsExists(appid);
        if (existResult.isResult()) {
            try {
                ExecutionParamsManager.ExecutionParamsModel modifiedModel = getExecutionParamsModelFromBody();
                modifiedModel.setId(existResult.getId());
                return ExecutionParamsPersistence.updateExecutionParams(appid, modifiedModel);
            } catch (IOException e) {
                throw new Utils.ServiceException("Invalid update data");
            }

        } else {
            String message = String.format("ExecParams does not exist for application: %s", appid);
            throw new Utils.ServiceException(message);
        }
    }


    @ApiOperation(value = "Deletes the configuration associated to the application",
            response = DeleteResult.class,
            responseContainer = "List")
    public Result deleteExecutionParams(@ApiParam(value = THE_APPLICATION_ID) String appid) {
        try {
            Object result = ExecutionParamsPersistence.deleteExecutionParams(appid);
            DeleteResult deleteResult = new DeleteResult((Boolean) result ? "deleted" : "delete_failed");
            return ok(Json.toJson(resultObject(deleteResult)));
        } catch (Exception e) {
            return getErrorResponse(e);
        }
    }

    @ApiResponses({
            @ApiResponse(code = 400, message = "Invalid applicationId is supplied.")
    })
    @ApiOperation(value = "Gets the the configuration for the application specified.",
            response = ApplicationManager.ApplicationModel.class)
    public Result getExecutionParams(@ApiParam(value = THE_APPLICATION_ID) String appid) {
        Optional<ExecutionParamsManager.ExecutionParamsModel> app = ExecutionParamsPersistence.getExecutionParams(appid);
        if (app.isPresent()) {
            ExecutionParamsManager.ExecutionParamsModel object= app.get();
            return ok(Json.toJson(resultObject(object)));
        } else
            return badRequest(Json.toJson(resultObject(new Utils.ServiceException("No execution params for app: " + appid))));//return ok(Json.toJson(resultObject(EMPTY)));
    }

    @ApiResponses({
            @ApiResponse(code = 400, message = "Invalid applicationId is supplied")
    })
    @ApiOperation(value = "Checks if the params for an application with the specified id exists",
            response = ExistResult.class)
    public Result executionParamsExists(@ApiParam(value = THE_APPLICATION_ID) String appid) {
        ExistResult existResult = ExecutionParamsPersistence.executionParamsExists(appid);
       /* if (existResult.isResult()){
            existResult.setId(appid);
        }*/
        return ok(Json.toJson(resultObject(existResult)));
    }

    @ApiOperation(value = "Returns a template 'json-chema' useful for editing execution params")
    public Result editorTemplate() {
        InputStream resourceAsStream = ExecutionParamsManager.class.getClassLoader().getResourceAsStream("execparams-create.json");
        return ok(resourceAsStream);
    }
}
