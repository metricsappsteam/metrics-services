package controllers;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.kibu.geoapis.metrics.MetricsCoreUtils;
import es.kibu.geoapis.metrics.actors.messages.MetricsMessage;
import es.kibu.geoapis.metrics.engine.core.BaseMetricsContext;
import es.kibu.geoapis.metrics.engine.core.BasicMetricsContext;
import es.kibu.geoapis.metrics.engine.core.Constants;
import es.kibu.geoapis.objectmodel.Metric;
import es.kibu.geoapis.objectmodel.MetricsDefinition;
import es.kibu.geoapis.objectmodel.Variable;
import es.kibu.geoapis.services.ExecutionParamsManager;
import es.kibu.geoapis.services.MetricsStorageUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import models.RunDetailsResult;
import models.Utils;
import models.persistence.MetricsPersistence;
import org.apache.commons.io.IOUtils;
import play.Configuration;
import play.Logger;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import static es.kibu.geoapis.services.objectmodel.results.ResultValue.resultObject;
import static models.Utils.metricsMessageShortDesc;

/**
 * Created by lrodriguez2002cu on 30/01/2017.
 */

@Api(value = "Run", description = "Operations related to the execution of metrics. Allow to execute metrics by different criteria.")

public class RunServices extends Controller {

    public static final String THE_APPLICATION_ID = "The application id";

    ActorSystem system;

    @Inject
    Configuration config;

    @Inject
    public RunServices(ActorSystem system) {
        this.system = system;
    }

    public class RunCommandResults implements Serializable {
        //TODO: include here the relevant info related to execution
        String logTopicKey;
        MetricsMessage metricsMessage;

        public RunCommandResults(){

        }

        public RunCommandResults(String logTopicKey, MetricsMessage metricsMessage) {
            this.logTopicKey = logTopicKey;
            this.metricsMessage = metricsMessage;
        }

        public String getLogTopicKey() {
            return logTopicKey;
        }

        public void setLogTopicKey(String logTopicKey) {
            this.logTopicKey = logTopicKey;
        }

        public MetricsMessage getMetricsMessage() {
            return metricsMessage;
        }

        public void setMetricsMessage(MetricsMessage metricsMessage) {
            this.metricsMessage = metricsMessage;
        }
    }

    @ApiOperation(value = "Return some details about metrics definition of the applications specified (i.e, the varibles, and the metrics). ",
            response = RunDetailsResult.class)
    public Result runDetails(@ApiParam(value = THE_APPLICATION_ID) String appid) {
        Optional<MetricsStorageUtils.MetricsDef> metricsDef = MetricsPersistence.getMetricsDef(appid);
        if (metricsDef.isPresent()) {
            try {
                RunDetailsResult result = RunDetailsResult.getRunDetailsResult(metricsDef.get());
                return ok(Json.toJson(resultObject(result)));

            } catch (Exception e) {
                e.printStackTrace();
                return badRequest(Json.toJson(resultObject(e)));
            }
        } else {
            return badRequest(Json.toJson(resultObject(new Utils.ServiceException("No metrics  found for app: " + appid))));
        }

    }



    /*

  "context": {
    "application": "",
    "session": "",
    "user": ""
  },
  "parameters": {
    "evaluation_mode": "evInmediateOnly",
    "variable": "",
    "mode": "variable-based"
  }
}
    * */

    public enum Mode {
        variable_based, metrics_based, render_based;

        public static Mode fromJsonString(String json) {
            if (json.equalsIgnoreCase("variable-based")) return Mode.variable_based;
            if (json.equalsIgnoreCase("metrics-based")) return Mode.metrics_based;
            if (json.equalsIgnoreCase("render-based")) return Mode.render_based;
            throw new RuntimeException(String.format("Invalid mode string provided: %s ", json));
        }
    }

    public static class Parameters {

        Mode mode;
        String variable;
        String metric;
        MetricsMessage.EvaluationMode evaluationMode;

        public Parameters() {
        }

        public Parameters(String variable, MetricsMessage.EvaluationMode evaluationMode) {
            this.variable = variable;
            this.evaluationMode = evaluationMode;
            this.mode = Mode.variable_based;
        }

        public Parameters(String metric) {
            this.metric = metric;
            this.mode = Mode.metrics_based;
        }

        public Mode getMode() {

            if (mode != null) {
                return mode;
            } else {
                //infer the mode
                if (variable != null) {
                    mode = Mode.variable_based;
                }
                if (metric != null) {
                    mode = Mode.metrics_based;
                }
                if (variable == null && metric == null) {
                    mode = Mode.render_based;
                }
            }
            assert mode != null;
            return mode;
        }

        public void setMode(Mode mode) {
            this.mode = mode;
        }

        public String getVariable() {
            return variable;
        }

        public void setVariable(String variable) {
            this.variable = variable;
        }

        public String getMetric() {
            return metric;
        }

        public void setMetric(String metric) {
            this.metric = metric;
        }

        public MetricsMessage.EvaluationMode getEvaluationMode() {
            return evaluationMode;
        }

        public void setEvaluationMode(MetricsMessage.EvaluationMode evaluationMode) {
            this.evaluationMode = evaluationMode;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Parameters that = (Parameters) o;

            if (getMode() != that.getMode()) return false;
            if (getVariable() != null ? !getVariable().equals(that.getVariable()) : that.getVariable() != null)
                return false;
            if (getMetric() != null ? !getMetric().equals(that.getMetric()) : that.getMetric() != null) return false;
            return getEvaluationMode() == that.getEvaluationMode();
        }

        @Override
        public int hashCode() {
            int result = getMode() != null ? getMode().hashCode() : 0;
            result = 31 * result + (getVariable() != null ? getVariable().hashCode() : 0);
            result = 31 * result + (getMetric() != null ? getMetric().hashCode() : 0);
            result = 31 * result + (getEvaluationMode() != null ? getEvaluationMode().hashCode() : 0);
            return result;
        }
    }

    public static class RunParams {

        BaseMetricsContext context;
        Parameters parameters;
        boolean saveResults = true;

        public BaseMetricsContext getContext() {
            return context;
        }

        public void setContext(BaseMetricsContext context) {
            this.context = context;
        }

        public Parameters getParameters() {
            return parameters;
        }

        public void setParameters(Parameters parameters) {
            this.parameters = parameters;
        }

        public RunParams() {
            context = new BasicMetricsContext();
            parameters = new Parameters();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            RunParams runParams = (RunParams) o;

            if (getContext() != null ? !getContext().equals(runParams.getContext()) : runParams.getContext() != null)
                return false;
            return parameters != null ? parameters.equals(runParams.parameters) : runParams.parameters == null;
        }

        @Override
        public int hashCode() {
            int result = getContext() != null ? getContext().hashCode() : 0;
            result = 31 * result + (parameters != null ? parameters.hashCode() : 0);
            return result;
        }

        public boolean isSaveResults() {
            return saveResults;
        }

        public void setSaveResults(boolean saveResults) {
            this.saveResults = saveResults;
        }
    }


    @ApiOperation(value = "Triggers the execution of the metrics of the given application based on the configuration specified.")
    public Result run(@ApiParam(value = THE_APPLICATION_ID)String appid) {
        try {
            JsonNode jsonNode = request().body().asJson();
            ObjectMapper mapper = Utils.getMapper();
            RunParams params = mapper.readValue(jsonNode.toString(), RunParams.class);
            //TODO: convert this to a possible security check in the future
            if (params.getContext().getApplicationId().equalsIgnoreCase(appid)) {
                MetricsMessage metricsMessage = Utils.metricsMessageFromParams(params);
                ActorRef frontendActorRef = getMetricsFrontendActorRef();
                Logger.info("Sending metrics request message to metrics frontend actor.. {}", metricsMessageShortDesc(metricsMessage));
                frontendActorRef.tell(metricsMessage, ActorRef.noSender());

                return ok(Json.toJson(resultObject(new RunCommandResults("log-topic-"+ metricsMessage.getId(), metricsMessage))));
            }
            else return badRequest(Json.toJson(resultObject(new Utils.ServiceException("Application can only be executed from their own context."))));

        } catch (IOException e) {
            return Utils.getErrorResponse(e);
        }

    }

    private ActorRef getMetricsFrontendActorRef() {
        String remoteActorAddress = getRemoteActorAddress(config);
        ActorRef ref = system.actorFor(remoteActorAddress);
        return ref;
    }

    public static String getRemoteActorAddress(Configuration config) {
        String frontendUrl = config.getString("metrics-frontend-url");
        String frontendActorSystem = config.getString("metrics-frontend-actor-system");
        String actorName = Constants.SCRIPT_ENGINE_FRONTEND_ACTOR_NAME;
        //"akka.tcp://HelloRemoteSystem@127.0.0.1:5150/user/RemoteActor"
        return String.format("akka.tcp://%s@%s/user/" + actorName, frontendActorSystem, frontendUrl);
    }


    private String quote(String toQuote) {
        return "\"" + toQuote + "\"";
    }

    private String getQuotedList(List<String> list) {
        String result = "";
        String sep = "";

        for (String s : list) {
            result += sep + quote(s);
            sep = ", ";
        }

        return result;
    }

    @ApiOperation(value = "Returns a template 'json-chema' useful for editing run params")
    public Result editorTemplate(@ApiParam(value = THE_APPLICATION_ID)String appid) {
        Optional<MetricsStorageUtils.MetricsDef> metricsDef = MetricsPersistence.getMetricsDef(appid);
        if (metricsDef.isPresent()) {
            try {
                RunDetailsResult runDetailsResult = RunDetailsResult.getRunDetailsResult(metricsDef.get());
                InputStream resourceAsStream = ExecutionParamsManager.class.getClassLoader().getResourceAsStream("runparams-create-parametrized.json");
                String content = IOUtils.toString(resourceAsStream, "UTF-8");
                String variablesList = getQuotedList(runDetailsResult.getVariables());
                String metricsList = getQuotedList(runDetailsResult.getMetrics());
                String result = String.format(content, variablesList, metricsList);
                return ok(result);

            } catch (Exception e) {
                return badRequest(Json.toJson(resultObject(e)));
            }

        } else {
            return badRequest(Json.toJson(resultObject(new Utils.ServiceException("No metrics  found for app: " + appid))));
        }
    }


    public Result test(){
        ActorRef metricsFrontendActorRef = getMetricsFrontendActorRef();
        metricsFrontendActorRef.tell("hello", ActorRef.noSender());
        return ok(metricsFrontendActorRef.path().name());
    }
}
