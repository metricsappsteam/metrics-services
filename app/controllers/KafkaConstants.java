package controllers;

public class KafkaConstants {

    public static final String KAFKA_DRY_RUN = "kafka-dry-run";
    public static final String TOPIC = "metrics_data";
    public static final String KAFKA_BOOTSTRAP_SERVERS = "kafka-bootstrap-servers";
}
