package controllers;

import play.Configuration;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.docs;

import javax.inject.Inject;

public class DocsController extends Controller {

    @Inject
    Configuration config;

    @Inject
    public DocsController() {
    }

    public Result docs() {
        return ok(docs.render());
    }


}
