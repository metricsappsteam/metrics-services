package controllers;

import akka.actor.ActorSystem;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import es.kibu.geoapis.backend.actors.persistence.CassandraPersistenceActor;
import es.kibu.geoapis.backend.actors.persistence.PersistenceActor;
import es.kibu.geoapis.metrics.engine.core.MetricRef;
import es.kibu.geoapis.metrics.engine.data.writing.NamingConventions;
import es.kibu.geoapis.services.MetricsStorageUtils;
import es.kibu.geoapis.services.data.DataSources;
import es.kibu.geoapis.services.objectmodel.results.ResultValue;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import models.RunDetailsResult;
import models.Utils;
import models.persistence.DataPersistence;
import models.persistence.MetricsPersistence;
import play.Configuration;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import scala.compat.java8.FutureConverters;
import scala.concurrent.Future;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import static es.kibu.geoapis.services.objectmodel.results.ResultValue.resultObject;

/**
 * Created by lrodr_000 on 26/04/2017.
 */

@Api(value = "DataServices", description = "Operations for accessing data of applications (i.e. metrics data stored, variable data stored, etc.)")
public class DataServices extends Controller {

    ActorSystem system;

    @Inject
    Configuration config;

    @Inject
    public DataServices(ActorSystem system) {
        this.system = system;
    }

    @ApiResponses({
            @ApiResponse(code = 400, message = "Invalid appid is supplied")
    })
    @ApiOperation(value = "Shows the datasources of a given application",response = DataSources.class)
    public Result getDataSources(String appid) {

        Optional<MetricsStorageUtils.MetricsDef> metricsDef = MetricsPersistence.getMetricsDef(appid);
        if (metricsDef.isPresent()) {

            try {
                DataSources dataSources = getDataSources(appid, metricsDef);

                return ok(Json.toJson(resultObject(dataSources)));

            } catch (Exception e) {
                return badRequest(Json.toJson(resultObject(e)));
            }
        } else {
            return badRequest(Json.toJson(resultObject(new Utils.ServiceException("No metrics definition found for app: " + appid))));
        }
    }

    private DataSources getDataSources(String appid, Optional<MetricsStorageUtils.MetricsDef> metricsDef) throws Exception {
        DataSources dataSources = new DataSources();
        RunDetailsResult result = RunDetailsResult.getRunDetailsResult(metricsDef.get());

        for (String metric : result.getMetrics()) {
            String cassandraTableNameFor = NamingConventions.getCassandraTableNameFor(new MetricRef(appid, metric));
            dataSources.addMetricDatasource(new DataSources.MetricsDatasourceInfo(metric, cassandraTableNameFor, appid));
        }

        for (String variable : result.getVariables()) {
            String tableName = NamingConventions.getVariableTableName(variable, metricsDef.get().getMetricId());
            dataSources.addVariableDatasource(new DataSources.VariablesDatasourceInfo(variable, tableName, appid));
        }
        return dataSources;
    }

    private DataSources.MetricsDatasourceInfo findMetric(String metricName, DataSources dataSources) {
        for (DataSources.MetricsDatasourceInfo metricsDatasourceInfo : dataSources.getMetricsDatasources()) {
            if (metricsDatasourceInfo.getMetricsName().equalsIgnoreCase(metricName)) return metricsDatasourceInfo;
        }
        return null;
    }

    private DataSources.VariablesDatasourceInfo findVariable(String variableName, DataSources dataSources) {
        for (DataSources.VariablesDatasourceInfo metricsDatasourceInfo : dataSources.getVariablesDatasources()) {
            if (metricsDatasourceInfo.getVariableName().equalsIgnoreCase(variableName)) return metricsDatasourceInfo;
        }
        return null;
    }


    @ApiResponses({
            @ApiResponse(code = 400, message = "Invalid applicationId is supplied or an error occurs while retrieving the data from the data store")
    })
    @ApiOperation(value = "Gets the data of a given metric given the application, " +
            "the metric name, and some filters. The filters allowed (through query string) at this time are, " +
            "session, and user, as the application can be extracted from the url.",
            response = ResultValue.class)
    public CompletionStage<Result> getDataForMetric(String appid, String metric) {
        Optional<MetricsStorageUtils.MetricsDef> metricsDef = MetricsPersistence.getMetricsDef(appid);
        if (metricsDef.isPresent()) {

            try {
                DataSources dataSources = getDataSources(appid, metricsDef);
                DataSources.MetricsDatasourceInfo metricInfo = findMetric(metric, dataSources);
                if (metricInfo != null) {
                    return getDataForDatasource(appid, metricInfo.getDatasourceName());
                } else throw new RuntimeException(String.format("metric %s not found for app: %s", metric, appid));

            } catch (Exception e) {
                return CompletableFuture.completedFuture(badRequest(Json.toJson(resultObject(e))));
            }
        } else {
            return CompletableFuture.completedFuture(badRequest(
                    Json.toJson(resultObject(new Utils.ServiceException("No metrics definition found for app: " + appid)))));
        }
    }


    @ApiResponses({
            @ApiResponse(code = 400, message = "Invalid applicationId is supplied or an error occurs while retrieving the data from the data store")
    })
    @ApiOperation(value = "Gets the data of a given variable given the application, " +
            "the variable name, and some filters. The filters allowed (through query string) at this time are, " +
            "session, and user, as the application can be extracted from the url.",
            response = ResultValue.class)
    public CompletionStage<Result> getDataForVariable(String appid, String variable) {
        Optional<MetricsStorageUtils.MetricsDef> metricsDef = MetricsPersistence.getMetricsDef(appid);

        if (metricsDef.isPresent()) {
            try {
                DataSources dataSources = getDataSources(appid, metricsDef);
                DataSources.VariablesDatasourceInfo variableInfo = findVariable(variable, dataSources);
                if (variableInfo != null) {
                    return getDataForDatasource(appid, variableInfo.getDatasourceName());
                } else throw new RuntimeException(String.format("variable %s not found for app: %s", variable, appid));

            } catch (Exception e) {
                return CompletableFuture.completedFuture(badRequest(Json.toJson(resultObject(e))));
            }
        } else {
            return CompletableFuture.completedFuture(badRequest(
                    Json.toJson(resultObject(new Utils.ServiceException("No metrics definition found for app: " + appid)))));
        }
    }
    private String getFullDatasource(String datasource){
        String keyspace = config.getString("cassandra.keyspace");
        return String.format("\"%s\".\"%s\"", keyspace, datasource);
    }


    @ApiResponses({
            @ApiResponse(code = 400, message = "Invalid applicationId is supplied or an error occurs while retrieving the data from the data store")
    })
    @ApiOperation(value = "Gets the data of a given datasource (i.e variable ot metric) given the application, " +
            "the datasource name, and some filters. The filters allowed (through query string) at this time are, " +
            "session, and user, as the application can be extracted from the url",
            response = ResultValue.class)

    public CompletionStage<Result> getDataForDatasource(String appid, String datasource) {

        //String datasource = request().getQueryString("datasource");
        String session = request().getQueryString("session");
        String user = request().getQueryString("user");

        DataPersistence persistence = getDataPersistence();

        Future<Object> dataFor = persistence.getDataFor(getFullDatasource(datasource), appid, session, user);

        ObjectMapper mapper = new ObjectMapper();

        CompletionStage<Object> objectCompletionStage = FutureConverters.toJava(dataFor);

        CompletionStage<Result> uCompletionStage = objectCompletionStage.thenApply(
                x -> {
                    try {
                        if (x instanceof es.kibu.geoapis.backend.actors.messages.Result) {
                            es.kibu.geoapis.backend.actors.messages.Result resultMessage = (es.kibu.geoapis.backend.actors.messages.Result) x;
                            if (resultMessage.isSuccess()) {
                                //move through the results

                                if (resultMessage.getResultValue() instanceof PersistenceActor.DBReadResult) {
                                    List<Object> results = ((PersistenceActor.DBReadResult) resultMessage.getResultValue()).getResultset();

                                    //List results = (List) resultMessage.getResultValue();
                                    ObjectNode json = Json.newObject();
                                    ArrayNode resultsArray = json.withArray("results");
                                    json.put("count", results.size());

                                    for (Object result : results) {
                                        if (result instanceof String) {
                                            JsonNode jsonNode = mapper.readTree((String) result);
                                            resultsArray.add(jsonNode);
                                        }
                                    }
                                    return ok(Json.toJson(resultObject(json)));
                                } else {
                                    return ok(Json.toJson(resultObject(resultMessage.getResultValue())));
                                }
                            } else {
                                String message = (resultMessage.getResultValue()!= null &&
                                        resultMessage.getResultValue() instanceof PersistenceActor.DBExceptionResult) ?
                                        ((PersistenceActor.DBExceptionResult)resultMessage.getResultValue()).getMessage():
                                        (resultMessage.getResultValue()!= null)? resultMessage.getResultValue().toString() : resultMessage.toString();

                                throw new RuntimeException(message);
                            }
                        } else {
                            throw new RuntimeException("The result obtained from persistent actor is not a valid result type");
                        }
                    } catch (Exception e) {
                        return badRequest(Json.toJson(resultObject(e)));
                    }
                }
        );

        return uCompletionStage;
    }

    private ResultValue getFailResult(String message) {
        ResultValue resultValue = new ResultValue("");
        resultValue.setErrorMessage(message);
        resultValue.setError(true);
        return resultValue;
    }

    private DataPersistence getDataPersistence() {


        return new DataPersistence(system, new CassandraPersistenceActor.CassandraSettings() {
            @Override
            public String getCassandraDBUser() {
                return config.getString("cassandra.user");
            }

            @Override
            public String getCassandraDBPass() {
                return config.getString("cassandra.password");
            }

            @Override
            public String getCassandraDBName() {
                return config.getString("cassandra.keyspace");
            }

            @Override
            public String getCassandraDBUri() {
                return config.getString("cassandra.uri");
            }

            @Override
            public void setCassandraDBName(String s) {

            }

            @Override
            public void setCassandraDBUri(String s) {

            }
        });
    }
}
