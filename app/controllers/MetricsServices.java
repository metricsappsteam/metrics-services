package controllers;

import akka.Done;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.kafka.ProducerSettings;
import akka.kafka.javadsl.Producer;
import akka.stream.ActorMaterializer;
import akka.stream.Materializer;
import akka.stream.OverflowStrategy;
import akka.stream.javadsl.RunnableGraph;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import akka.stream.javadsl.SourceQueueWithComplete;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.uuid.impl.UUIDUtil;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import es.kibu.geoapis.backend.actors.messages.Identifiable;
import es.kibu.geoapis.backend.actors.persistence.PersistenceActor;
import es.kibu.geoapis.backend.actors.persistence.PersistenceMessageUtils;
import es.kibu.geoapis.data.DataUtils;
import es.kibu.geoapis.metrics.MetricUtils;
import es.kibu.geoapis.metrics.actors.data.MetricsDefinitionCacheActor;
import es.kibu.geoapis.metrics.actors.data.MetricsDefinitionCacheActorConsumer;
import es.kibu.geoapis.objectmodel.MetricsDefinition;
import es.kibu.geoapis.objectmodel.Variable;
import es.kibu.geoapis.serialization.MetricsDefinitionReader;
import es.kibu.geoapis.services.MetricsStorageUtils;
import es.kibu.geoapis.services.objectmodel.results.*;
import io.swagger.annotations.*;
import models.Utils;
import models.persistence.ApplicationPersistence;
import models.persistence.ExecutionParamsPersistence;
import models.persistence.MetricsPersistence;
import modules.SubmitActor;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.joda.time.DateTime;
import play.Configuration;
import play.Logger;
import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Result;
import scala.compat.java8.FutureConverters;
import views.html.index;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import static es.kibu.geoapis.services.MetricsManager.getHash;
import static es.kibu.geoapis.services.objectmodel.results.ResultValue.resultObject;
import static models.persistence.MetricsPersistence.deleteMetricsDef;

import static akka.pattern.Patterns.ask;

@Api(value = "Metrics", description = "Operations related to metrics (i.e. creation , update, remove, execution)")
public class MetricsServices extends Controller {

    public static final String KAFKA_DRY_RUN = "kafka-dry-run";
    private static final String TOPIC = "metrics_data";
    private static final String GETS_THE_METRICS_DEFINITION_OF_A_GIVEN_APPLICATION = "Gets the metrics definition of a given application";
    private static final String APPLICATION_ID_OF_THE_METRICS = "Application id of the metrics";
    private static final String KAFKA_BOOTSTRAP_SERVERS = "kafka-bootstrap-servers";
    public static final String DATA_SUBMITTED = "data_submitted";

    ActorSystem system;
    final Materializer materializer;

    @Inject
    HttpExecutionContext ec;

    @Inject
    Configuration config;

    @Inject
    public MetricsServices(ActorSystem system, @Named(SubmitActor.SUBMIT_ACTOR_NAME) ActorRef submitActor) {
        this.system = system;
        ExecutionParamsPersistence.setSystem(system);
        ApplicationPersistence.setSystem(system);
        MetricsPersistence.setSystem(system);

        this.submitActor = submitActor;
        materializer = ActorMaterializer.create(system);

    }

    public Result index() {
        return ok(index.render("Your new application is ready."));
    }

    @ApiOperation(value = "Allow checking that the metric definition exists for an application",
            response = ExistResult.class)
    public Result metricsExist(String appid) {
        ExistResult existResult = MetricsPersistence.metricsExistsForApp(appid);
        return ok(Json.toJson(resultObject(existResult)));
    }

    @ApiOperation(value = "Test method to help checking all is ok",
            response = MetricsStorageUtils.MetricsDef.class,
            responseContainer = "List")
    public Result test(@ApiParam(value = APPLICATION_ID_OF_THE_METRICS) String appid) {
        Logger.info("Test service was called for: " + appid);
        return play.mvc.Results.TODO;
    }






    @ApiOperation(value = "Tests that the submission works ok")
    /**
     *
     * {
     "experiment":"experiment1",
     "application":"app-36437104577c4432",
     "timeid":"${timeid}",
     "session":"session10",
     "location":{
     "accuracy":45.0,
     "altitude":185.0,
     "latitude":39.99409897,
     "longitude":-0.07352183,
     "time":"2017-09-14T14:38:45.000+02:00"
     },
     "previd":"${previd}",
     "time":"2017-09-14T14:43:58.279+02:00",
     "user":"luis10"
     }
     */
    public CompletionStage<Result> testSubmission(@ApiParam(value = APPLICATION_ID_OF_THE_METRICS) String appid) {

        Logger.info("Test service was called for: " + appid);

        if (appid.length() > 20) {
            return CompletableFuture.completedFuture(
                    badRequest(Json.toJson(resultObject(new Utils.ServiceException("No metrics for app: " + appid))))
            );
        }

        String template = "{\n" +
                "     \"application\":\"%s\",\n" +
                "     \"timeid\":\"%s\",\n" +
                "     \"session\":\"sessionTest\",\n" +
                "     \"previd\":\"%s\",\n" +
                "     \"dummy\": 1,\n" +
                "     \"time\":\"%s\",\n" +
                "     \"user\":\"userTest\"\n" +
                "     }";

        Optional<MetricsStorageUtils.MetricsDef> metricsDef = MetricsPersistence.getMetricsDef(appid);
        if (metricsDef.isPresent()) {
            String nowUUID = es.kibu.geoapis.data.Utils.newTimeUUID().toString();
            String timeString = DateTime.now().toDateTimeISO().toString();
            String content = String.format(template, appid, nowUUID, nowUUID, timeString);
            return submitDataInternal(appid, "testVar", content);

        } else {
            Logger.warn("There is not test application setup.");
            return CompletableFuture.completedFuture(badRequest(Json.toJson(resultObject(new Utils.ServiceException("No metrics for app: " + appid)))));
        }

    }


    private MetricsResult toMetricsResult(MetricsStorageUtils.MetricsDef definition) {
        return new MetricsResult(definition.getContent(), definition.getHash(), definition.getMetricId(), definition.getIsPublic());
    }

    @ApiResponses({
            @ApiResponse(code = 400, message = "Invalid applicationId is supplied")
    })
    @ApiOperation(value = GETS_THE_METRICS_DEFINITION_OF_A_GIVEN_APPLICATION,
            response = MetricsStorageUtils.MetricsDef.class,
            responseContainer = "List")
    public Result metrics(@ApiParam(value = APPLICATION_ID_OF_THE_METRICS) String appid) {
        Optional<MetricsStorageUtils.MetricsDef> metricsDef = MetricsPersistence.getMetricsDef(appid);
        if (metricsDef.isPresent()) {
            MetricsResult metricsResult = toMetricsResult(metricsDef.get());
            return ok(Json.toJson(resultObject(metricsResult)));
        } else
            return badRequest(Json.toJson(resultObject(new Utils.ServiceException("No metrics for app: " + appid))));//return ok(Json.toJson(resultObject(EMPTY)));
    }

    @ApiOperation(value = "Updates the metric definition of a given application",
            response = UpdateResult.class,
            responseContainer = "List")
    public Result updateMetrics(@ApiParam(value = APPLICATION_ID_OF_THE_METRICS) String appid) {
        try {
            Object result = doUpdate(appid);
            boolean updated = !PersistenceMessageUtils.isError((PersistenceActor.QueryResult) result);
            UpdateResult updateResult = new UpdateResult(updated ? "updated" : "update_failed");

            return ok(Json.toJson(resultObject(updateResult)));
        } catch (Exception e) {
            return getErrorResponse(e);
        }
    }

    @ApiOperation(value = "Deletes the metrics definition of a given application",
            response = DeleteResult.class,
            responseContainer = "List")
    public Result deleteMetrics(@ApiParam(value = APPLICATION_ID_OF_THE_METRICS) String appid) {
        try {
            Object result = deleteMetricsDef(appid);
            DeleteResult deleteResult = new DeleteResult((Boolean) result ? "deleted" : "delete_failed");
            return ok(Json.toJson(resultObject(deleteResult)));
        } catch (Exception e) {
            return getErrorResponse(e);
        }
    }

    private static ProducerSettings<String, String> producerSettings;

    private ProducerSettings getProducerSettings() {
        if (producerSettings == null) {
            String bootstrapServers = config.getString(KAFKA_BOOTSTRAP_SERVERS);
            Logger.debug("boostrap server configured: {}", bootstrapServers);
            producerSettings = ProducerSettings
                    .create(system, new StringSerializer(), new StringSerializer())
                    .withBootstrapServers(bootstrapServers);
        }
        return producerSettings;
    }

    private static String getTopicKey(String variableName, String applicationName) {
        return String.format("%s:%s", applicationName, variableName);
    }

    public static class VariableData implements Serializable {
        String key;
        String content;

        public VariableData(String key, String content) {
            this.key = key;
            this.content = content;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public VariableData() {
        }
    }

    public Optional<MetricsStorageUtils.MetricsDef> getMetricsDef(String appId) {
        return MetricsPersistence.getMetricsDef(appId);
    }

    private boolean isDryRun() {
        String dryrun = request().getQueryString("dryrun");
        boolean result = Boolean.valueOf(dryrun) || config.getBoolean(KAFKA_DRY_RUN, false);
        if (result) Logger.debug("Kafka producer 'dry run' is enable");
        return result;
    }
/*

    @ApiOperation(value = "Submits metric data for the specified application and variable",
            response = SubmitResult.class
    )
    public CompletableFuture<Result> submitData1(@ApiParam(value = APPLICATION_ID_OF_THE_METRICS) String appid,
                                                 @ApiParam(value = "The variable to which the data belongs") String variable) {
        try {

            Logger.debug("Receiving data for variable {}", variable);
            //checkStrictMode(appid, variable);
            String content = request().body().asJson().toString();
            Logger.debug("Content Received variable: " + variable + " content:" + content);

            final String topicName = getTopicKey(variable, appid);

            CompletionStage<Void> completionStage = CompletableFuture.runAsync(new Runnable() {
                @Override
                public void run() {
                    getSubmitActor().tell(new VariableData(topicName, content), ActorRef.noSender());
                    throw new RuntimeException("this must fail");
                    //getSubmitCompletionStage(materializer, content, topicName);
                }
            });

            CompletableFuture<Result> result = completionStage.toCompletableFuture().handleAsync((done1, throwable) -> {
                if (throwable != null) {
                    Logger.error("Error while sending kafka producer record:", throwable);
                    return badRequest(Json.toJson(resultObject(throwable)));
                } else {
                    SubmitResult submitResult = new SubmitResult();
                    submitResult.setValue("data_submitted");
                    return ok(Json.toJson(resultObject(submitResult)));
                }
            });

            return result;

        } catch (Exception e) {
            Logger.error("Error while submitting data: " + e.getMessage(), e);
            return getErrorResponseFuture(e);
        }

    }
*/

    ActorRef submitActor;

    public CompletionStage<Result> submitData(@ApiParam(value = APPLICATION_ID_OF_THE_METRICS) String appid,
                                                @ApiParam(value = "The variable to which the data belongs") String variable) {

        final String content = request().body().asJson().toString();
        return submitDataInternal(appid, variable, content);
    }

    private CompletionStage<Result> submitDataInternal(@ApiParam(value = APPLICATION_ID_OF_THE_METRICS) String appid, @ApiParam(value = "The variable to which the data belongs") String variable, String content) {
        final String topicName = getTopicKey(variable, appid);

        VariableData data = new VariableData(topicName, content);
        return FutureConverters.toJava(ask(submitActor, data, 1000))
                .thenApply(response -> {
                    SubmitActor.SubmitResultData resultData = (SubmitActor.SubmitResultData) response;
                    SubmitActor.SubmitResult result = resultData.getResult();
                    if (result == SubmitActor.SubmitResult.SUBMITTED) {
                        SubmitResult submitResult = new SubmitResult();
                        submitResult.setValue(DATA_SUBMITTED);
                        return ok(Json.toJson(resultObject(submitResult)));
                    }
                    else if (result == SubmitActor.SubmitResult.FAILED) {
                        return badRequest(Json.toJson(resultObject(resultData.getReason())));
                    }
                    throw new RuntimeException(String.format("Impossible to happen this on service, Result:%s", result.name()));
                });
    }


    //Al this point there is not way of invalidating this cache
    private Map<String, String> applicationAndVariables = new HashMap<>();

    String getKey(String application, String variable) {
        return String.format("%s%s", application, variable);
    }

    private void checkStrictMode(String appid, String variable) {

        String key = getKey(appid, variable);
        if (!applicationAndVariables.containsKey(key)) {
            Optional<MetricsStorageUtils.MetricsDef> metricsDef = getMetricsDef(appid);
            if (metricsDef.isPresent()) {
                MetricsDefinition metricsDefinition = null;
                try {
                    metricsDefinition = MetricUtils.withMetricDefinitionFromContent(metricsDef.get().getContent());
                    final Variable variableByName = metricsDefinition.getVariableByName(variable);
                    if (variableByName == null) {
                        throw new Utils.ServiceException("Variable not found for application metrics definition");
                    }
                    applicationAndVariables.put(key, appid);
                } catch (ProcessingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                throw new Utils.AppNotFoundException(appid);
            }

        }

    }

    private RunnableGraph<ActorRef> runnableGraphToKafkaActor() {
        //new VariableData(topicName, content)
        RunnableGraph<ActorRef> to = Source.actorRef(0, OverflowStrategy.fail())
                .map(rawElem -> {
                    VariableData elem = (VariableData) rawElem;
                    Logger.debug("Sending data to topic: {}, key: {}", TOPIC, elem.getKey());
                    return new ProducerRecord<>(TOPIC, elem.getKey(), elem.getContent());
                })
                .to(createKafkaSink());
        return to; //.runWith(createKafkaSink(), materializer);
    }


    private RunnableGraph<SourceQueueWithComplete<Object>> runnableGraphToKafkaQueue() {
        //new VariableData(topicName, content)
        RunnableGraph<SourceQueueWithComplete<Object>> to = Source.queue(0, OverflowStrategy.dropTail())
                .map(rawElem -> {
                    VariableData elem = (VariableData) rawElem;
                    Logger.debug("Sending data to topic: {}, key: {}", TOPIC, elem.getKey());
                    return new ProducerRecord<>(TOPIC, elem.getKey(), elem.getContent());
                })
                .to(createKafkaSink());
        return to; //.runWith(createKafkaSink(), materializer);
    }


    public CompletableFuture<Result> notifyCacheActors(Materializer materializer, String application, String metricId) {

        CompletionStage<Done> completionStage = getUpdateMetricsCompletionStage(materializer, application, metricId);

        return notifyCacheActors(completionStage);
    }

    public CompletableFuture<Result> notifyCacheActorsInvalidateAll() {
        CompletionStage<Done> completionStage = getUpdateMetricsCompletionStage(materializer, MetricsDefinitionCacheActor.MetricsDefinitionActorMessage.createInvalidateAllMessage());
        return notifyCacheActors(completionStage);
    }

    public CompletableFuture<Result> notifyCacheActorsInvalidateApp(String appid) {
        CompletionStage<Done> completionStage = getUpdateMetricsCompletionStage(materializer, MetricsDefinitionCacheActor.MetricsDefinitionActorMessage.createInvalidateMessage(appid, null));
        return notifyCacheActors(completionStage);
    }

    private CompletableFuture<Result> notifyCacheActors(CompletionStage<Done> completionStage) {
        CompletableFuture<Result> result = completionStage.toCompletableFuture().handleAsync((done1, throwable) -> {
            if (throwable != null) {
                Logger.debug("Error while sending kafka record (Update Metrics):", throwable);
                return badRequest(Json.toJson(resultObject(throwable)));
            } else {
                SubmitResult submitResult = new SubmitResult();
                submitResult.setValue(DATA_SUBMITTED);
                return ok(Json.toJson(resultObject(submitResult)));
            }
        });

        return result;
    }

    private CompletionStage<Done> getUpdateMetricsCompletionStage(Materializer materializer, String application, String metricId) {
        //if (!testing()) {
        MetricsDefinitionCacheActor.MetricsDefinitionActorMessage msg = MetricsDefinitionCacheActor.MetricsDefinitionActorMessage.createInvalidateMessage(application, metricId);
        return getUpdateMetricsCompletionStage(materializer, msg);
    }

    private Sink<ProducerRecord<String, String>, CompletionStage<Done>> kafkaSink;


    private Sink<ProducerRecord<String, String>, CompletionStage<Done>> createKafkaSink() {
        return createKafkaSink(false);
    }

    private Sink<ProducerRecord<String, String>, CompletionStage<Done>> createKafkaSink(boolean fake) {
        boolean artificial = fake || false;
        if (kafkaSink == null || artificial) {
            if (artificial) {
                kafkaSink = Sink.foreach(param -> {
                    throw new RuntimeException(String.format("Failed while receiving key: %s value: %s", param.key(), param.value()));
                    //Logger.debug(String.format("Sink received key: %s value: %s", param.key(), param.value()));
                });
            } else kafkaSink = Producer.plainSink(getProducerSettings());
        }
        return kafkaSink;
    }


    private CompletionStage<Done> getUpdateMetricsCompletionStage(Materializer materializer, MetricsDefinitionCacheActor.MetricsDefinitionActorMessage msg) {
        getProducerSettings();

        return Source.single(msg)
                .map(elem -> {
                    String key = String.format("%s:%s", msg.getApplication(), msg.getMetricId());
                    Logger.debug("Sending invalidate to topic: {}, key: {}", MetricsDefinitionCacheActorConsumer.TOPIC_METRICS_DEFINITION_CHANGE, key);
                    return new ProducerRecord<>(MetricsDefinitionCacheActorConsumer.TOPIC_METRICS_DEFINITION_CHANGE, key, elem.toJson());
                })
                .runWith(createKafkaSink(), materializer);
        //}
    }


    public boolean testing() {
        return isDryRun();
    }


    @ApiOperation(value = "Creates the metric of a given application",
            response = CreateResult.class,
            responseContainer = "List")
    public Result createMetrics(@ApiParam(value = APPLICATION_ID_OF_THE_METRICS) String appid) {
        try {
            Object result = doCreate(appid);
            boolean wasOk = result instanceof Identifiable;// !PersistenceMessageUtils.isError((PersistenceActor.QueryResult) result);
            if (wasOk) {
                String id = (String) ((Identifiable) result).getId();
                CreateResult created = new CreateResult("created", id);
                return ok(Json.toJson(resultObject(created)));
            } else {
                return badRequest(Json.toJson(resultObject(PersistenceMessageUtils.getErrorMsg((PersistenceActor.QueryResult) result))));
            }

        } catch (Exception e) {
            return getErrorResponse(e);
        }
    }

    public static Result getErrorResponse(Exception e) {
        return Utils.getErrorResponse(e);
    }

    public static CompletableFuture<Result> getErrorResponseFuture(Exception e) {
        return CompletableFuture.completedFuture(Utils.getErrorResponse(e));//badRequest(Json.toJson(resultObject(e)));
    }

    private Object doUpdate(String appid) throws ProcessingException, NoSuchAlgorithmException {

        ExistResult existResult = MetricsPersistence.metricsExistsForApp(appid);
        if (existResult.isResult()) {
            JsonNode jsonNode = request().body().asJson();
            String content = jsonNode.toString();
            MetricsDefinitionReader reader = new MetricsDefinitionReader();
            MetricsDefinition definition = reader.readFrom(content);
            String metricId = definition.getId();
            String hash = getHash(content);

            MetricsStorageUtils.MetricsDef metricsDef = new MetricsStorageUtils.MetricsDef(content, hash, metricId, true);
            metricsDef.setId(existResult.getId());

            Object result = MetricsPersistence.updateMetricsDef(appid, metricsDef);
            boolean updated = !PersistenceMessageUtils.isError((PersistenceActor.QueryResult) result);
            if (updated) {
                //TODO: This has really to be improved, as it is creating the source stream element by element.
                //TODO: Clearly there should be a (MUCH) better way.
                notifyCacheActors(materializer, appid, metricId);
            }
            return result;
        } else throw new Utils.ServiceException("Metrics does not exist for application");
    }

    private Object doCreate(String appid) throws ProcessingException, NoSuchAlgorithmException {
        ExistResult existResult = MetricsPersistence.metricsExistsForApp(appid);
        if (!existResult.isResult()) {
            JsonNode jsonNode = request().body().asJson();
            MetricsDefinitionReader reader = new MetricsDefinitionReader();
            String content = jsonNode.toString();
            MetricsDefinition definition = reader.readFrom(content);
            String metricId = definition.getId();
            String hash = getHash(content);

            MetricsStorageUtils.MetricsDef metricsDef = new MetricsStorageUtils.MetricsDef(content, hash, metricId, true);

            return MetricsPersistence.createMetricsDef(appid, metricsDef);
        } else {
            throw new Utils.ServiceException("Metrics already exist for application");
        }
    }

    @ApiOperation(value = "Returns the metrics schema. This is useful mainly for editing the schema definitions files.")
    public Result metricsSchema() {
        return ok(MetricsDefinition.class.getResourceAsStream("/metrics-schema.json"));
    }


    @ApiOperation(value = "Invalidates all the caches in the system. Its is useful for debugging etc.")
    public CompletableFuture<Result> invalidateMetricsCaches() {
        return notifyCacheActorsInvalidateAll().toCompletableFuture();
    }

}
