package controllers;

import akka.actor.ActorSystem;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import es.kibu.geoapis.backend.actors.messages.Identifiable;
import es.kibu.geoapis.backend.actors.persistence.PersistenceActor;
import es.kibu.geoapis.backend.actors.persistence.PersistenceMessageUtils;
import es.kibu.geoapis.metrics.engine.data.writing.NamingConventions;
import es.kibu.geoapis.services.ApplicationManager;
import es.kibu.geoapis.services.objectmodel.results.*;
import io.swagger.annotations.*;
import models.Utils;
import models.persistence.ApplicationPersistence;
import models.persistence.ExecutionParamsPersistence;
import models.persistence.MetricsPersistence;
import play.Logger;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static es.kibu.geoapis.services.objectmodel.results.ResultValue.resultObject;

/**
 * Created by lrodriguez2002cu on 14/01/2017.
 */

@Api(value = "Applications", description = "Operations related to applications (i.e. creation , update, delete)")
public class ApplicationServices extends Controller {

    public static final String THE_APPLICATION_ID = "The application id";

    ActorSystem system;

    @Inject
    public ApplicationServices(ActorSystem system) {
        this.system = system;

        ApplicationPersistence.setSystem(system);
        ExecutionParamsPersistence.setSystem(system);
        MetricsPersistence.setSystem(system);
    }


    @ApiResponses({
            @ApiResponse(code = 400, message = "Invalid applicationId is supplied")
    })
    @ApiOperation(value = "Gets the application by its id",
            response = ApplicationManager.ApplicationModel.class,
    responseContainer = "List")
    public Result getApplications() {
        Optional<List<ApplicationManager.ApplicationModel>> app = ApplicationPersistence.getApplications();
        if (app.isPresent()) {
            List<ApplicationManager.ApplicationModel> applications = app.get();
            return ok(Json.toJson(resultObject(applications)));
        } else
            return badRequest(Json.toJson(resultObject(new Utils.ServiceException("No applications found: "))));//return ok(Json.toJson(resultObject(EMPTY)));
    }


    @ApiOperation(value = "Obtains the version of the services")
    public Result versionInfo() {
        return ok(services.BuildInfo.toJson());
    }

    @ApiOperation(value = "Creates an application",
            response = CreateResult.class,
            responseContainer = "List")

    public Result createApplication() {
        Logger.debug("Creating application");
        try {
            Object result = doCreate();
            boolean wasOk = result instanceof Identifiable;// !PersistenceMessageUtils.isError((PersistenceActor.QueryResult) result);
            if (wasOk) {
                //String id = (String) ((Identifiable) result).getId();
                String id  =((ApplicationManager.ApplicationModel)result).getAppid();
                CreateResult created = new CreateResult("created", id);
                return ok(Json.toJson(resultObject(created)));
            } else {
                return badRequest(Json.toJson(resultObject(PersistenceMessageUtils.getErrorMsg((PersistenceActor.QueryResult) result))));
            }

        } catch (Exception e) {
            return getErrorResponse(e);
        }
    }

    private Result getErrorResponse(Exception e) {
        return Utils.getErrorResponse(e);
    }



    @ApiOperation(value = "Updates the data associates to a given application",
            response = UpdateResult.class,
            responseContainer = "List")
    public Result updateApplication(@ApiParam(value = THE_APPLICATION_ID) String appid) {
        try {
            Object result = doUpdate(appid);
            boolean updated = !PersistenceMessageUtils.isError((PersistenceActor.QueryResult) result);
            UpdateResult updateResult = new UpdateResult(updated ? "updated" : "update_failed");
            return ok(Json.toJson(resultObject(updateResult)));
        } catch (Exception e) {
            return getErrorResponse(e);
        }
    }

    private Object doCreate() throws ProcessingException, NoSuchAlgorithmException {

        Logger.debug("Creating application (doCreate)");
        JsonNode jsonNode = request().body().asJson();
        ObjectMapper mapper = new ObjectMapper();

        try {
            Application app = mapper.readValue(jsonNode.toString(), Application.class);
            ApplicationManager.ApplicationModel applicationModel = new ApplicationManager.ApplicationModel();
            applicationModel.fromApp(app);
            String appid = NamingConventions.ApplicationIdCreator.createId();
            applicationModel.setAppid(appid);
            return ApplicationPersistence.createApplication(appid, applicationModel);
        } catch (IOException e) {
            throw new Utils.ServiceException("Invalid application data format");

        }
    }

    private Object doUpdate(String appid) throws ProcessingException, NoSuchAlgorithmException {
        ExistResult existResult = ApplicationPersistence.applicationExists(appid);
        if (existResult.isResult()) {
            try {
                JsonNode jsonNode = request().body().asJson();
                ObjectMapper mapper = new ObjectMapper();
                Application app = mapper.readValue(jsonNode.toString(), Application.class);
                ApplicationManager.ApplicationModel applicationModel = new ApplicationManager.ApplicationModel();
                applicationModel.fromApp(app);
                applicationModel.setId(existResult.getId());
                return ApplicationPersistence.updateApplication(appid, applicationModel);
            } catch (IOException e) {
                throw new Utils.ServiceException("Invalid update data");
            }

        } else throw new Utils.ServiceException("Metrics does not exist for application");
    }


    @ApiOperation(value = "Deletes a given application",
            response = DeleteResult.class,
            responseContainer = "List")
    public Result deleteApplication(@ApiParam(value = THE_APPLICATION_ID) String appid) {
        try {
            Object result = ApplicationPersistence.deleteApplication(appid);
            DeleteResult deleteResult = new DeleteResult((Boolean) result ? "deleted" : "delete_failed");
            return ok(Json.toJson(resultObject(deleteResult)));
        } catch (Exception e) {
            return getErrorResponse(e);
        }
    }

    @ApiResponses({
            @ApiResponse(code = 400, message = "Invalid applicationId is supplied")
    })
    @ApiOperation(value = "Gets the application by its id",
            response = ApplicationManager.ApplicationModel.class)
    public Result getApplication(@ApiParam(value = THE_APPLICATION_ID) String appid) {
        Optional<ApplicationManager.ApplicationModel> app = ApplicationPersistence.getApplication(appid);
        if (app.isPresent()) {
            Application application = app.get();
            return ok(Json.toJson(resultObject(application)));
        } else
            return badRequest(Json.toJson(resultObject(new Utils.ServiceException("No metrics for app: " + appid))));//return ok(Json.toJson(resultObject(EMPTY)));
    }

    @ApiResponses({
            @ApiResponse(code = 400, message = "Invalid applicationId is supplied")
    })
    @ApiOperation(value = "Checks if the application with the specified id exists",
            response = ExistResult.class)
    public Result applicationExists(String appid) {
        ExistResult existResult = ApplicationPersistence.applicationExists(appid);
        if (existResult.isResult()){
            existResult.setId(appid);
        }
        return ok(Json.toJson(resultObject(existResult)));
    }

    @ApiOperation(value = "Returns a template 'json-chema' useful for editing applications")
    public Result editorTemplate() {
        InputStream resourceAsStream = ApplicationManager.class.getClassLoader().getResourceAsStream("application-create.json");
        return ok(resourceAsStream);
    }
}
