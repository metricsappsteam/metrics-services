package models;

import es.kibu.geoapis.metrics.MetricsCoreUtils;
import es.kibu.geoapis.objectmodel.Metric;
import es.kibu.geoapis.objectmodel.MetricsDefinition;
import es.kibu.geoapis.objectmodel.Variable;
import es.kibu.geoapis.services.MetricsStorageUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lrodriguez2002cu on 26/04/2017.
 */
public class RunDetailsResult implements Serializable {

    List<String> metrics;
    List<String> variables;

    public RunDetailsResult(List<String> metrics, List<String> variables) {
        this.metrics = metrics;
        this.variables = variables;
    }

    public RunDetailsResult() {
        this.metrics = new ArrayList<>();
        this.variables = new ArrayList<>();
    }

    public List<String> getMetrics() {
        return metrics;
    }

    public void setMetrics(List<String> metrics) {
        this.metrics = metrics;
    }

    public List<String> getVariables() {
        return variables;
    }

    public void setVariables(List<String> variables) {
        this.variables = variables;
    }


    public static RunDetailsResult getRunDetailsResult(MetricsStorageUtils.MetricsDef metricsDef) throws Exception {

        RunDetailsResult result = new RunDetailsResult();
        String content = metricsDef.getContent();
        MetricsDefinition definition = MetricsCoreUtils.withMetricDefinitionFromContent(content);
        for (Metric metric : definition.getMetrics()) {
            result.getMetrics().add(metric.getName());
        }

        for (Variable variable : definition.getVariables()) {
            result.getVariables().add(variable.getName());
        }
        return result;
    }

}
