package models.persistence;

import akka.actor.ActorSystem;
import es.kibu.geoapis.services.ApplicationManager;
import es.kibu.geoapis.services.ExecutionParamsManager;
import es.kibu.geoapis.services.ExecutionParamsStorageUtils;
import es.kibu.geoapis.services.objectmodel.results.ExistResult;

import java.util.Optional;

import static play.libs.Akka.system;

/**
 * Created by lrodr_000 on 14/01/2017.
 */
public class ExecutionParamsPersistence {

    static ActorSystem system;

    static ExecutionParamsManager executionParamsManager = null;
    public static void setSystem(ActorSystem system){
        ExecutionParamsPersistence.system = system;
        ExecutionParamsPersistence.executionParamsManager = null;
    }

    private static ExecutionParamsManager getExecutionParamsManager() {
        assert system != null;
        if (executionParamsManager== null){
            executionParamsManager = ExecutionParamsStorageUtils.getExecutionParamsManager(system);
        }
        return executionParamsManager;
    }

    public static Optional<ExecutionParamsManager.ExecutionParamsModel> getExecutionParams(String applicationId) {
        return getExecutionParamsManager().getExecutionParams(applicationId);
    }

    public static Object updateExecutionParams(String applicationId, ExecutionParamsManager.ExecutionParamsModel executionParams) {
        return getExecutionParamsManager().updateExecutionParams(applicationId, executionParams);
    }

    public static Object deleteExecutionParams(String applicationId) {
        return getExecutionParamsManager().deleteExecutionParams(applicationId);
    }


    public static Object createExecutionParams(String applicationId,  ExecutionParamsManager.ExecutionParamsModel executionParams) {
        return getExecutionParamsManager().createExecutionParams(applicationId,  executionParams);
    }

    public static ExistResult executionParamsExists(String applicationId) {
        return getExecutionParamsManager().executionParamsExists(applicationId);
    }

}
