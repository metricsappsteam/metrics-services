package models.persistence;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.pattern.Patterns;
import akka.util.Timeout;
import es.kibu.geoapis.backend.actors.persistence.AbstractQueries;
import es.kibu.geoapis.backend.actors.persistence.CassandraPersistenceActor;
import org.joda.time.Interval;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.concurrent.Future;

import java.util.concurrent.TimeUnit;

/**
 * Created by lrodr_000 on 26/04/2017.
 */
public class DataPersistence {

    Logger logger = LoggerFactory.getLogger(DataPersistence.class);

    Timeout timeout = Timeout.apply(15, TimeUnit.SECONDS);

    ActorSystem system;
    CassandraPersistenceActor.CassandraSettings settings;

    ActorRef getDataPersistenceActorRef() {
        Props props = CassandraPersistenceActor.props(settings);
        return system.actorOf(props);
    }

    public DataPersistence(ActorSystem system, CassandraPersistenceActor.CassandraSettings settings) {
        this.system = system;
        this.settings = settings;
    }

    public Future<Object> getDataFor(String datasource, String application, String session, String user) {

        AbstractQueries.Query dataQuery = getDataQuery(datasource, application, session, user);

        //get the reference to the persistent actor
        ActorRef actorRef = getDataPersistenceActorRef();
        return Patterns.ask(actorRef, dataQuery, timeout);
    }

    private AbstractQueries.Query getDataQuery(String datasource, String application, String session, String user) {
        return  getDataQuery(datasource, application, session, user, null);
    }

    private AbstractQueries.Query getDataQuery(String datasource, String application, String session, String user, Interval interval) {

        AbstractQueries.Query q = new AbstractQueries.SelectQuery(String.class, /*"\"metrics\".\"var_movementspeedMetricsNacho\""*/ datasource);
        q.withFilter("application", AbstractQueries.ComparisonOperators.EQUAL, /*"app-47f2e1b5a3c44404"*/ application);

        if (user != null && !user.equalsIgnoreCase("*"))
            q.withFilter("user", AbstractQueries.ComparisonOperators.EQUAL, /*"user1")*/ user);

        if (session != null && !session.equalsIgnoreCase("*"))
            q.withFilter("session", AbstractQueries.ComparisonOperators.EQUAL, /*"session1"*/ session);

        if (interval != null) {
            q.withFilter("time", AbstractQueries.ComparisonOperators.GTE, new AbstractQueries.QueryValue<>(interval.getStart()));
            q.withFilter("time", AbstractQueries.ComparisonOperators.LTE, new AbstractQueries.QueryValue<>(interval.getEnd()));
        }

        //q.withFilter("time", AbstractQueries.ComparisonOperators.LT, new AbstractQueries.QueryValue<DateTime>(DateTime.parse("2017-04-11T09:52:39.773Z")));
        //q.sortBy("time", AbstractQueries.SortDirection.DESC);
        logger.debug("Prepared Query: {}", q);
        return q;
    }


}
