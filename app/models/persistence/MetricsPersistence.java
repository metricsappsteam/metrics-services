package models.persistence;

import akka.actor.ActorSystem;
import es.kibu.geoapis.services.MetricsManager;
import es.kibu.geoapis.services.MetricsStorageUtils;
import es.kibu.geoapis.services.MetricsStorageUtils.MetricsDef;
import es.kibu.geoapis.services.objectmodel.results.ExistResult;

import java.util.Optional;

import static play.libs.Akka.system;

/**
 * Created by lrodriguez2002cu on 25/12/2016.
 */
public class MetricsPersistence {

    static ActorSystem system;

    private static MetricsManager getMetricsManager() {

        assert system!= null;
        return MetricsStorageUtils.getMetricsManager(system);
    }

    public static void setSystem(ActorSystem system) {
        MetricsPersistence.system = system;
    }

    public static Optional<MetricsDef> getMetricsDef(String applicationId) {
        return getMetricsManager().getMetricsDef(applicationId);
    }

    public static Object updateMetricsDef(String applicationId, MetricsDef metricsDef) {
        return getMetricsManager().updateMetricsDef(applicationId, metricsDef);
    }

    public static Object deleteMetricsDef(String applicationId) {
        return getMetricsManager().deleteMetricsDef(applicationId);
    }


    public static Object createMetricsDef(String applicationId, MetricsDef metricsDef) {
        return getMetricsManager().createMetricsDef(applicationId, metricsDef);
    }

    public static ExistResult metricsExistsForApp(String applicationId) {
        return getMetricsManager().metricsExistsForApp(applicationId);
    }

}

