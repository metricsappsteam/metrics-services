package models.persistence;

import akka.actor.ActorSystem;
import es.kibu.geoapis.services.ApplicationManager;
import es.kibu.geoapis.services.ApplicationStorageUtils;
import es.kibu.geoapis.services.MetricsStorageUtils;
import es.kibu.geoapis.services.objectmodel.results.ExistResult;


import java.util.List;
import java.util.Optional;

import static play.libs.Akka.system;

/**
 * Created by lrodr_000 on 14/01/2017.
 */
public class ApplicationPersistence {

    static ActorSystem system;

    public static void setSystem(ActorSystem system){
        ApplicationPersistence.system = system;
    }

    private static ApplicationManager getApplicationManager() {
        assert system!= null;
        return ApplicationStorageUtils.getApplicationManager(system);
    }

    public static Optional<ApplicationManager.ApplicationModel> getApplication(String applicationId) {
        return getApplicationManager().getApplication(applicationId);
    }

    public static Optional<List<ApplicationManager.ApplicationModel>> getApplications() {
        return getApplicationManager().getApplications();
    }

    public static Object updateApplication(String applicationId, ApplicationManager.ApplicationModel application) {
        return getApplicationManager().updateApplication(applicationId, application);
    }

    public static Object deleteApplication(String applicationId) {
        return getApplicationManager().deleteApplication(applicationId);
    }


    public static Object createApplication(String applicationId, ApplicationManager.ApplicationModel applicationModel) {
        return getApplicationManager().createApplication(applicationId, applicationModel);
    }

    public static ExistResult applicationExists(String applicationId) {
        return getApplicationManager().applicationExists(applicationId);
    }

}
