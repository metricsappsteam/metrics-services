package models;

import akka.actor.ActorSystem;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.module.SimpleModule;
import controllers.RunServices;
import es.kibu.geoapis.metrics.actors.messages.MetricsMessage;
import es.kibu.geoapis.metrics.engine.core.BaseMetricsContext;
import es.kibu.geoapis.metrics.engine.core.BasicMetricsContext;
import es.kibu.geoapis.services.ApplicationManager;
import es.kibu.geoapis.services.ApplicationStorageUtils;
import es.kibu.geoapis.services.objectmodel.results.ExistResult;
import play.libs.Json;
import play.mvc.Result;

import java.io.IOException;

import static es.kibu.geoapis.services.objectmodel.results.ResultValue.resultObject;
import static play.mvc.Results.badRequest;

/**
 * Created by lrodr_000 on 14/01/2017.
 */
public class Utils {

    public static Result getErrorResponse(Exception e) {
        return badRequest(Json.toJson(resultObject(e)));
    }

    public static class ServiceException extends RuntimeException {

        public ServiceException(String message) {
            super(message);
        }
    }

    public static class AppNotFoundException extends ServiceException {

        public static final String APP_NOT_FOUND = "APP_NOT_FOUND";

        public AppNotFoundException(String appid) {
            super(APP_NOT_FOUND + ":" +appid);
        }
    }

    public static void checkApplicationExist(ActorSystem system, String applicationId) {
        ApplicationManager manager = ApplicationStorageUtils.getApplicationManager(system);
        ExistResult existResult = manager.applicationExists(applicationId);
        if (!existResult.isResult()) {
            throw new ServiceException(String.format("Application does not exist: '%s'", applicationId));
        }
    }


    public static class BaseMetricsContextSerializer extends JsonSerializer<BaseMetricsContext> {
        @Override
        public void serialize(BaseMetricsContext contextValue, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
            jsonGenerator.writeObject((BasicMetricsContext)contextValue);
        }
    }

    public static class BaseMetricsContextDeserializer extends JsonDeserializer<BaseMetricsContext> {

        @Override
        public BaseMetricsContext deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            BasicMetricsContext basicMetricsContext = jsonParser.readValueAs(BasicMetricsContext.class);
            return basicMetricsContext;
        }
    }


/*
    public static class ParametersSerializer extends JsonSerializer<RunServices.Parameters> {
        @Override
        public void serialize(RunServices.Parameters contextValue, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
            jsonGenerator.writeObject(contextValue);
        }
    }

    public static class ParametersDeserializer extends JsonDeserializer<RunServices.Parameters> {
        @Override
        public RunServices.Parameters deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            BasicMetricsContext basicMetricsContext = jsonParser.readValueAs(BasicMetricsContext.class);
            return basicMetricsContext;
        }
    }
*/


    public static class BaseContextSerializationModule extends SimpleModule
    {
        public BaseContextSerializationModule() {
            super("BaseContextSerializationModule"/*, new Version(0,0,1,null)*/);
            //context.setMixInAnnotations(Target.class, MixIn.class);
            //this.addSerializer(BaseMetricsContext.class, new BaseMetricsContextSerializer());
            this.addDeserializer(BaseMetricsContext.class, new BaseMetricsContextDeserializer());
        }

    }


    public static ObjectMapper getMapper(){
        ObjectMapper mapper =  new  ObjectMapper();
        mapper.registerModule(new BaseContextSerializationModule());
        return mapper;
    }

    public static  MetricsMessage metricsMessageFromParams(RunServices.RunParams  params){
        BaseMetricsContext context = params.getContext();
        MetricsMessage result;
        switch (params.getParameters().getMode()) {

            case variable_based:
                result = MetricsMessage.forVariable(null, params.getParameters().getVariable(),
                        params.getParameters().getEvaluationMode() == MetricsMessage.EvaluationMode.evImmediateOnly,  context, "");
                break;
            case metrics_based:
                result = MetricsMessage.forMetric(null, params.getParameters().getMetric(), context, "");
                break;
            case render_based:
                result = MetricsMessage.forRender(null, context, "");
                break;
             default:
                 throw new RuntimeException("Invalid  mode found: " +  params.getParameters().getMode().name());
        }

        result.getEngineConfig().setSaveData(params.isSaveResults());

        return  result;
    }

    public static String metricsMessageShortDesc(MetricsMessage metricsMessage) {

        return String.format("MetricsMessage: {app: %s, session: %s, user:%s, metric: %s, variable: %s , evalMode: %s,  config: { save: %s, notifications: %s} }"
                , metricsMessage.getContext().getApplicationId()
                , metricsMessage.getContext().getSessionId()
                , metricsMessage.getContext().getUserId()
                , metricsMessage.getMetric()
                , metricsMessage.getVariable()
                , metricsMessage.getEvaluationMode()
                , metricsMessage.getEngineConfig().isSaveData()
                , metricsMessage.getEngineConfig().isEnableActionsNotifications()


        );
    }

}
