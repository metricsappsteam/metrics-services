package filters;

/**
 * Created by lrodr_000 on 14/03/2017.
 */
import play.filters.cors.CORSFilter;
import play.http.DefaultHttpFilters;
import play.mvc.EssentialFilter;
import play.http.HttpFilters;
import javax.inject.Inject;

public class Filters extends DefaultHttpFilters {

    private LoggingFilter loggingFilter;
    private CORSFilter corsFilter;
    @Inject
    public Filters(LoggingFilter gzip, CORSFilter corsFilter) {
        super(corsFilter);
        this.loggingFilter = gzip;
        this.corsFilter = corsFilter;
    }

    @Override
    public EssentialFilter[] filters() {
        return new EssentialFilter[] { loggingFilter, corsFilter.asJava()};
    }
}